export default async (url: string, init = {}): Promise<any> => {
    const response = await fetch(url, init);
    return await response.json();
};
