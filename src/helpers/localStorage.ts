import { UserState } from "../reducers/types";

const loadState = () => {
    try {
        const serializedState = localStorage.getItem("user-preferences");
        if (serializedState === null) {
            return undefined;
        }
        return JSON.parse(serializedState);
    } catch (err) {
        return undefined;
    }
};

const saveState = (state: UserState) => {
    try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem("user-preferences", serializedState);
    } catch {
        // ignore write errors
    }
};

export default {
    loadState,
    saveState,
};
