export default async (locale: string): Promise<Record<string, string>> => {
    let messages = {};

    switch (locale) {
        case "ru":
            messages = await import("../i18n/ru.json");
            break;

        default:
            messages = await import("../i18n/en.json");
    }

    return messages;
};
