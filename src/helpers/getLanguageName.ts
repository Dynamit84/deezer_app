import LOCALES from "../i18n/locales";

export default (langCode: string): string => {
    const locale = LOCALES.filter((locale) => locale.code === langCode);

    return locale[0].name;
};
