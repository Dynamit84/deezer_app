export const genresUrl = "/genre";
export const getGenreArtistsUrl = (genreId: number): string =>
    `/genre/${genreId}/artists`;
export const getArtistInfoUrl = (artistId: number): string =>
    `/artist/${artistId}`;
export const getArtistAlbumsUrl = (
    artistId: number,
    params: string = ""
): string => `/artist/${artistId}/albums${params}`;
export const getArtistSimilarUrl = (
    artistId: number,
    params: string = ""
): string => `/artist/${artistId}/related${params}`;
export const getArtistPlaylistsUrl = (
    artistId: number,
    params: string = ""
): string => `/artist/${artistId}/playlists${params}`;
export const getArtistCommentsUrl = (
    artistId: number,
    params: string = ""
): string => `/artist/${artistId}/comments${params}`;
export const getArtistMostPlayedUrl = (artistId: number): string =>
    `/artist/${artistId}/top?limit=50`;
export const getAlbumUrl = (albumId: number): string => `/album/${albumId}`;
export const getPlaylistUrl = (playlistId: number): string =>
    `/playlist/${playlistId}`;
export const getUserUrl = (userId: number): string => `/user/${userId}`;
export const getUserFollowersUrl = (
    userId: number,
    params: string = ""
): string => `/user/${userId}/followers${params}`;
export const getUserFollowingsUrl = (
    userId: number,
    params: string = ""
): string => `/user/${userId}/followings${params}`;
export const getUserAlbumsUrl = (userId: number, params: string = ""): string =>
    `/user/${userId}/albums${params}`;
export const getUserPlaylistsUrl = (
    userId: number,
    params: string = ""
): string => `/user/${userId}/playlists${params}`;
export const getUserArtistsUrl = (
    userId: number,
    params: string = ""
): string => `/user/${userId}/artists${params}`;
export const getSearchArtistsUrl = (
    search: string,
    params: string = ""
): string => `/search/artist?${search ? `q=${search}` : params}`;
export const getSearchPlaylistsUrl = (
    search: string,
    params: string = ""
): string => `/search/playlist?${search ? `q=${search}` : params}`;
export const getSearchAlbumsUrl = (
    search: string,
    params: string = ""
): string => `/search/album?${search ? `q=${search}` : params}`;
export const getSearchTracksUrl = (
    search: string,
    params: string = ""
): string => `/search/track?${search ? `q=${search}` : params}`;
