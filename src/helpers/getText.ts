export const getText = (entity: string): string =>
    `app.${entity.replace(/user_|search_/gi, "").toLowerCase()}`;
