import { Album } from "../types";

export function formatNumber(num: number): string | 0 {
    return num && num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}

export const formatDate = (date: string): string =>
    date.split("-").reverse().join("/");

export const convertDuration = (time: number): string => {
    const minutes = Math.floor(time / 60);
    let seconds: string | number = time - minutes * 60;
    return `${minutes}:${seconds < 10 ? (seconds = `0${seconds}`) : seconds}`;
};

export function desc(
    a: { [key: string]: any },
    b: { [key: string]: any },
    orderBy: string
) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

export function stableSort(array: unknown[], cmp: Function) {
    const stabilizedThis = array.map((el: any, index: number) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = cmp(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
}

export function getSorting(order: string, orderBy: string) {
    return order === "desc"
        ? (a: Object, b: Object) => desc(a, b, orderBy)
        : (a: Object, b: Object) => -desc(a, b, orderBy);
}

export const formatDuration = (
    duration: number,
    formatMessage: Function
): string => {
    let hours = Math.floor(duration / 3600);
    duration %= 3600;
    let minutes = String(Math.floor(duration / 60)).padStart(2, "0");

    return `${
        hours ? `${hours} ${formatMessage({ id: "app.hour" })} ` : ""
    } ${minutes} ${formatMessage({ id: "app.minutes" })}`;
};

export const formAlbumInfoData = (
    { nb_tracks, release_date, duration, fans }: Album,
    formatMessage: Function
): string[] => [
    `${nb_tracks} tracks`,
    formatDuration(duration as number, formatMessage),
    formatDate(release_date as string),
    `${formatNumber(fans as number)} fans`,
];

type Params = {
    nb_tracks: number;
    creation_date: string;
    duration: number;
    fans: number;
};
export const formPlaylistInfoData = (
    { nb_tracks, creation_date, duration, fans }: Params,
    formatMessage: Function
) => [
    `${formatMessage({ id: "app.tracks" }).toLowerCase()} - ${nb_tracks}`,
    formatDuration(duration, formatMessage),
    formatDate(creation_date.split(" ")[0]),
    `${formatNumber(fans)} ${formatMessage({ id: "app.fans" }).toLowerCase()}`,
];
