import { Reducer } from "redux";
import {
    FETCH_USER_PROFILE_FAILS,
    FETCH_USER_PROFILE_SUCCESS,
    FETCH_USER_FOLLOWERS_FAILS,
    FETCH_USER_FOLLOWERS_SUCCESS,
    FETCH_USER_FOLLOWINGS_FAILS,
    FETCH_USER_FOLLOWINGS_SUCCESS,
    FETCH_USER_ALBUMS_FAILS,
    FETCH_USER_ALBUMS_SUCCESS,
    FETCH_USER_PLAYLISTS_SUCCESS,
    FETCH_USER_PLAYLISTS_FAILS,
    FETCH_USER_ARTISTS_FAILS,
    FETCH_USER_ARTISTS_SUCCESS,
    FETCH_USER_PROFILE,
    FETCH_USER_ARTISTS,
    FETCH_USER_PLAYLISTS,
    FETCH_USER_ALBUMS,
    FETCH_USER_FOLLOWERS,
    FETCH_USER_FOLLOWINGS,
    RESET_PROFILE_STATE,
} from "../actions/actionTypes";
import { ProfileState } from "./types";
import { ERROR_MSG } from "../constants/texts";

const regex = /\?(.*)/gi;

const defaultState: ProfileState = {
    info: {
        data: {
            name: "",
            picture_medium: "",
            id: 0,
        },
        isLoading: false,
    },
    albums: {
        data: [],
        isLoading: false,
        total: null,
        isFirstLoad: true,
    },
    playlists: {
        data: [],
        total: null,
        isLoading: false,
        isFirstLoad: true,
    },
    followings: {
        data: [],
        total: null,
        isLoading: false,
        isFirstLoad: true,
    },
    followers: {
        data: [],
        total: null,
        isLoading: false,
        isFirstLoad: true,
        next: "",
    },
    artists: {
        data: [],
        total: null,
        isLoading: false,
        isFirstLoad: true,
    },
};
const profileReducer: Reducer<ProfileState> = (
    state: ProfileState = defaultState,
    { type, payload }
) => {
    switch (type) {
        case FETCH_USER_PROFILE_SUCCESS:
            state = {
                ...state,
                info: { ...defaultState.info, data: payload, isLoading: false },
            };
            break;
        case FETCH_USER_PROFILE:
            state = {
                ...state,
                info: {
                    ...state.info,
                    isLoading: true,
                },
            };
            break;
        case FETCH_USER_PROFILE_FAILS:
            state = {
                ...state,
                info: {
                    errorMsg: ERROR_MSG,
                    ...defaultState.info,
                },
            };
            break;

        case FETCH_USER_FOLLOWERS_SUCCESS:
            state = {
                ...state,
                followers: state.followers.isFirstLoad
                    ? {
                          data: payload.data,
                          next: payload.next && payload.next.match(regex)[0],
                          isLoading: false,
                          isFirstLoad: false,
                          total: payload.total,
                      }
                    : {
                          ...state.followers,
                          data: [...state.followers.data, ...payload.data],
                          next: payload.next && payload.next.match(regex)[0],
                          isLoading: false,
                          total: payload.total,
                      },
            };
            break;
        case FETCH_USER_FOLLOWERS:
            state = {
                ...state,
                followers: {
                    ...state.followers,
                    isLoading: true,
                },
            };
            break;
        case FETCH_USER_FOLLOWERS_FAILS:
            state = {
                ...state,
                followers: {
                    ...defaultState.followers,
                    errorMsg: ERROR_MSG,
                },
            };
            break;

        case FETCH_USER_FOLLOWINGS_SUCCESS:
            state = {
                ...state,
                followings: state.followings.isFirstLoad
                    ? {
                          data: payload.data,
                          next: payload.next && payload.next.match(regex)[0],
                          isLoading: false,
                          isFirstLoad: false,
                          total: payload.total,
                      }
                    : {
                          ...state.followings,
                          data: [...state.followings.data, ...payload.data],
                          next: payload.next && payload.next.match(regex)[0],
                          isLoading: false,
                          total: payload.total,
                      },
            };
            break;
        case FETCH_USER_FOLLOWINGS:
            state = {
                ...state,
                followings: {
                    ...state.followings,
                    isLoading: true,
                },
            };
            break;
        case FETCH_USER_FOLLOWINGS_FAILS:
            state = {
                ...state,
                followings: {
                    ...defaultState.followings,
                    errorMsg: ERROR_MSG,
                },
            };
            break;
        case FETCH_USER_ALBUMS_SUCCESS:
            state = {
                ...state,
                albums: state.albums.isFirstLoad
                    ? {
                          data: payload.data,
                          next: payload.next && payload.next.match(regex)[0],
                          isLoading: false,
                          isFirstLoad: false,
                          total: payload.total,
                      }
                    : {
                          ...state.albums,
                          data: [...state.albums.data, ...payload.data],
                          next: payload.next && payload.next.match(regex)[0],
                          isLoading: false,
                          total: payload.total,
                      },
            };
            break;
        case FETCH_USER_ALBUMS:
            state = {
                ...state,
                albums: {
                    ...state.albums,
                    isLoading: true,
                },
            };
            break;
        case FETCH_USER_ALBUMS_FAILS:
            state = {
                ...state,
                albums: {
                    errorMsg: ERROR_MSG,
                    ...defaultState.albums,
                },
            };
            break;
        case FETCH_USER_PLAYLISTS_SUCCESS:
            state = {
                ...state,
                playlists: state.playlists.isFirstLoad
                    ? {
                          data: payload.data,
                          next: payload.next && payload.next.match(regex)[0],
                          isLoading: false,
                          isFirstLoad: false,
                          total: payload.total,
                      }
                    : {
                          ...state.playlists,
                          data: [...state.playlists.data, ...payload.data],
                          next: payload.next && payload.next.match(regex)[0],
                          isLoading: false,
                          total: payload.total,
                      },
            };
            break;
        case FETCH_USER_PLAYLISTS:
            state = {
                ...state,
                playlists: {
                    ...state.playlists,
                    isLoading: true,
                },
            };
            break;
        case FETCH_USER_PLAYLISTS_FAILS:
            state = {
                ...state,
                playlists: {
                    errorMsg: ERROR_MSG,
                    ...defaultState.playlists,
                },
            };
            break;
        case FETCH_USER_ARTISTS_SUCCESS:
            state = {
                ...state,
                artists: state.artists.isFirstLoad
                    ? {
                          data: payload.data,
                          next: payload.next && payload.next.match(regex)[0],
                          isLoading: false,
                          isFirstLoad: false,
                          total: payload.total,
                      }
                    : {
                          ...state.artists,
                          data: [...state.artists.data, ...payload.data],
                          next: payload.next && payload.next.match(regex)[0],
                          isLoading: false,
                          total: payload.total,
                      },
            };
            break;
        case FETCH_USER_ARTISTS:
            state = {
                ...state,
                artists: {
                    ...state.artists,
                    isLoading: true,
                },
            };
            break;
        case FETCH_USER_ARTISTS_FAILS:
            state = {
                ...state,
                artists: {
                    errorMsg: ERROR_MSG,
                    ...defaultState.artists,
                },
            };
            break;
        case RESET_PROFILE_STATE:
            state = {
                ...defaultState,
            };
            break;
        default:
            return state;
    }
    return state;
};

export default profileReducer;
