import { Reducer } from "redux";
import { PLAYER_LOADED } from "../actions/actionTypes";
import { PlayerState } from "./types";
import { PlayerLoadedAction } from "../actions/types";

const defaultState: PlayerState = {
    isLoaded: false,
};
const playerReducer: Reducer<PlayerState> = (
    state: PlayerState = defaultState,
    { type }: PlayerLoadedAction
) => {
    switch (type) {
        case PLAYER_LOADED:
            state = {
                ...state,
                isLoaded: true,
            };
            break;
        default:
            return state;
    }
    return state;
};

export default playerReducer;
