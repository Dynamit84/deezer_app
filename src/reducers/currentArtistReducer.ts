import { Reducer } from "redux";
import {
    FETCH_ARTIST_INFO_SUCCESS,
    FETCH_ARTIST_INFO_FAILS,
    FETCH_ARTIST_COMMENTS_SUCCESS,
    FETCH_ARTIST_COMMENTS_FAILS,
    FETCH_ARTIST_DISCOGRAPHY_FAILS,
    FETCH_ARTIST_DISCOGRAPHY_SUCCESS,
    FETCH_ARTIST_MOSTPLAYED_FAILS,
    FETCH_ARTIST_MOSTPLAYED_SUCCESS,
    FETCH_ARTIST_PLAYLISTS_FAILS,
    FETCH_ARTIST_PLAYLISTS_SUCCESS,
    FETCH_ARTIST_SIMILAR_FAILS,
    FETCH_ARTIST_SIMILAR_SUCCESS,
    FETCH_ARTIST_SIMILAR,
    FETCH_ARTIST_MOSTPLAYED,
    FETCH_ARTIST_DISCOGRAPHY,
    FETCH_ARTIST_INFO,
    FETCH_ARTIST_COMMENTS,
    FETCH_ARTIST_PLAYLISTS,
    RESET_ARIST_STATE,
} from "../actions/actionTypes";
import { ERROR_MSG } from "../constants/texts";
import { CurrentArtistState } from "./types";
import { Artist } from "../types";

const regex = /\?(.*)/gi;

const defaultState = {
    info: {
        data: {} as Artist,
        isLoading: false,
    },
    playlists: {
        data: [],
        isLoading: false,
        isFirstLoad: true,
    },
    discography: {
        data: [],
        isLoading: false,
        isFirstLoad: true,
    },
    comments: {
        data: [],
        isLoading: false,
        isFirstLoad: true,
    },
    similar: {
        data: [],
        isLoading: false,
        isFirstLoad: true,
    },
    mostPlayed: {
        data: [],
        isLoading: false,
        isFirstLoad: true,
    },
};
const currentArtistsReducer: Reducer<CurrentArtistState> = (
    state: CurrentArtistState = defaultState,
    { type, payload }
) => {
    switch (type) {
        case FETCH_ARTIST_INFO_SUCCESS:
            state = {
                ...state,
                info: {
                    data: payload,
                    isLoading: false,
                },
            };
            break;
        case FETCH_ARTIST_INFO:
            state = {
                ...state,
                info: {
                    ...state.info,
                    isLoading: true,
                },
            };
            break;
        case FETCH_ARTIST_INFO_FAILS:
            state = {
                ...state,
                info: {
                    ...defaultState.info,
                    errorMsg: ERROR_MSG,
                },
            };
            break;
        case FETCH_ARTIST_COMMENTS_SUCCESS:
            state = {
                ...state,
                comments: {
                    data: payload.data,
                    isLoading: false,
                    isFirstLoad: false,
                },
            };
            break;
        case FETCH_ARTIST_COMMENTS_FAILS:
            state = {
                ...state,
                comments: {
                    ...defaultState.comments,
                    errorMsg: ERROR_MSG,
                },
            };
            break;
        case FETCH_ARTIST_COMMENTS:
            state = {
                ...state,
                comments: {
                    ...state.comments,
                    isLoading: true,
                },
            };
            break;
        case FETCH_ARTIST_DISCOGRAPHY:
            state = {
                ...state,
                discography: {
                    ...state.discography,
                    isLoading: true,
                },
            };
            break;
        case FETCH_ARTIST_DISCOGRAPHY_SUCCESS:
            state = {
                ...state,
                discography: state.discography.isFirstLoad
                    ? {
                          data: payload.data,
                          next: payload.next && payload.next.match(regex)[0],
                          isLoading: false,
                          isFirstLoad: false,
                      }
                    : {
                          ...state.discography,
                          data: [...state.discography.data, ...payload.data],
                          next: payload.next && payload.next.match(regex)[0],
                          isLoading: false,
                      },
            };
            break;
        case FETCH_ARTIST_DISCOGRAPHY_FAILS:
            state = {
                ...state,
                discography: {
                    ...defaultState.discography,
                    errorMsg: ERROR_MSG,
                },
            };
            break;
        case FETCH_ARTIST_MOSTPLAYED_SUCCESS:
            state = {
                ...state,
                mostPlayed: {
                    data: payload.data,
                    isLoading: false,
                    isFirstLoad: false,
                },
            };
            break;
        case FETCH_ARTIST_MOSTPLAYED_FAILS:
            state = {
                ...state,
                mostPlayed: {
                    ...defaultState.mostPlayed,
                    errorMsg: ERROR_MSG,
                },
            };
            break;
        case FETCH_ARTIST_MOSTPLAYED:
            state = {
                ...state,
                mostPlayed: {
                    ...state.mostPlayed,
                    isLoading: true,
                },
            };
            break;
        case FETCH_ARTIST_PLAYLISTS_SUCCESS:
            state = {
                ...state,
                playlists: state.playlists.isFirstLoad
                    ? {
                          data: payload.data,
                          next: payload.next && payload.next.match(regex)[0],
                          isLoading: false,
                          isFirstLoad: false,
                      }
                    : {
                          ...state.playlists,
                          data: [...state.playlists.data, ...payload.data],
                          next: payload.next && payload.next.match(regex)[0],
                          isLoading: false,
                      },
            };
            break;
        case FETCH_ARTIST_PLAYLISTS_FAILS:
            state = {
                ...state,
                playlists: {
                    ...defaultState.playlists,
                    errorMsg: ERROR_MSG,
                },
            };
            break;
        case FETCH_ARTIST_PLAYLISTS:
            state = {
                ...state,
                playlists: {
                    ...state.playlists,
                    isLoading: true,
                },
            };
            break;
        case FETCH_ARTIST_SIMILAR:
            state = {
                ...state,
                similar: {
                    ...state.similar,
                    isLoading: true,
                },
            };
            break;
        case FETCH_ARTIST_SIMILAR_SUCCESS:
            state = {
                ...state,
                similar: state.similar.isFirstLoad
                    ? {
                          data: payload.data,
                          next: payload.next && payload.next.match(regex)[0],
                          isLoading: false,
                          isFirstLoad: false,
                      }
                    : {
                          ...state.similar,
                          data: [...state.similar.data, ...payload.data],
                          next: payload.next && payload.next.match(regex)[0],
                          isLoading: false,
                      },
            };
            break;
        case FETCH_ARTIST_SIMILAR_FAILS:
            state = {
                ...state,
                similar: {
                    ...defaultState.similar,
                    errorMsg: ERROR_MSG,
                },
            };
            break;
        case RESET_ARIST_STATE:
            state = {
                ...defaultState,
            };
            break;

        default:
            return state;
    }
    return state;
};

export default currentArtistsReducer;
