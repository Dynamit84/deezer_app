import { Genre, Album, Artist, Track, User, GeneralData } from "../../types";
import { PaletteType } from "@material-ui/core";

export type CurrentlyPayingState = {
    trackId: number;
    type: string;
    entityId: number | null;
    isPlaying: boolean;
};

export type GenresState = {
    musicGenres: Genre[];
    currentGenre: Genre;
    isLoading: boolean;
    errorMsg?: string;
};

export type PlayerState = { isLoaded: boolean };

export type PlaylistState = {
    tracks: {
        data: Track[];
    };
    isLoading: boolean;
    errorMsg?: string;
    creator?: {
        id: number;
        name: string;
    };
};

export type InfoSlice = {
    data: User;
    isLoading: boolean;
    errorMsg?: string;
};

export type ProfileState = {
    info: InfoSlice;
    albums: GeneralData;
    playlists: GeneralData;
    followings: GeneralData;
    followers: GeneralData;
    artists: GeneralData;
};

export type SearchState = {
    albums: GeneralData;
    artists: GeneralData;
    playlists: GeneralData;
    tracks: GeneralData;
    isAllDataLoading: boolean;
    errorMsg?: string;
};

export type ArtistsState = {
    data: Artist[];
    isLoading: boolean;
    errorMsg?: string;
};

export type CurrentArtistState = {
    info: {
        data: Artist;
        isLoading: boolean;
        errorMsg?: string;
    };
    playlists: GeneralData;
    discography: GeneralData;
    comments: GeneralData;
    similar: GeneralData;
    mostPlayed: GeneralData;
};

export type ApplicationState = {
    genres: GenresState;
    artists: ArtistsState;
    currentArtist: CurrentArtistState;
    currentlyPlaying: CurrentlyPayingState;
    player: PlayerState;
    album: Album;
    playlist: PlaylistState;
    profile: ProfileState;
    search: SearchState;
    user: UserState;
};

type ThemeProps = {
    bgColorMain: string;
    bgColorSecondary: string;
    color: string;
};

export type Themes = {
    dark: ThemeProps;
    light: ThemeProps;
    [key: string]: any;
};

export type UserState = {
    language: string;
    theme: PaletteType;
};
