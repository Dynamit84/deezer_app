import { Reducer } from "redux";
import {
    FETCH_ARTISTS_SUCCESS,
    FETCH_ARTISTS_FAILS,
    FETCH_ARTISTS,
    RESET_ARISTS_STATE,
} from "../actions/actionTypes";
import { ERROR_MSG } from "../constants/texts";
import { ArtistsState } from "./types";

const defaultState: ArtistsState = {
    data: [],
    isLoading: false,
};
const artistsReducer: Reducer<ArtistsState> = (
    state: ArtistsState = defaultState,
    { type, payload }
) => {
    switch (type) {
        case FETCH_ARTISTS_SUCCESS:
            state = {
                ...state,
                data: payload.data,
                isLoading: false,
            };
            break;
        case FETCH_ARTISTS_FAILS:
            state = {
                ...defaultState,
                errorMsg: ERROR_MSG,
            };
            break;
        case FETCH_ARTISTS:
            state = {
                ...state,
                isLoading: true,
            };
            break;
        case RESET_ARISTS_STATE:
            state = {
                ...defaultState,
            };
            break;
        default:
            return state;
    }
    return state;
};

export default artistsReducer;
