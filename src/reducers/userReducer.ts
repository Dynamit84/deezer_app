import { UserState } from "./types/index";
import { Reducer } from "redux";
import { CHANGE_LANGUAGE, TOGGLE_THEME } from "../actions/actionTypes";
import { UserActions } from "../actions/types";
import LOCALES from "../i18n/locales";

const defaultState: UserState = { language: LOCALES[0].code, theme: "light" };
const userReducer: Reducer<UserState, UserActions> = (
    state = defaultState,
    action: UserActions
) => {
    switch (action.type) {
        case CHANGE_LANGUAGE:
            state = {
                ...state,
                language: action.payload,
            };
            break;

        case TOGGLE_THEME:
            state = {
                ...state,
                theme: state.theme === "dark" ? "light" : "dark",
            };
            break;
        default:
            return state;
    }
    return state;
};

export default userReducer;
