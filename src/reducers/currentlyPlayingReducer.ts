import { Reducer } from "redux";
import { CurrentlyPayingState } from "./types";
import { CLICKED_PLAY_BUTTON } from "../actions/actionTypes";
import { ClickedPlayAction } from "../actions/types";

const defaultState: CurrentlyPayingState = {
    trackId: 0,
    type: "",
    entityId: null,
    isPlaying: false,
};
const currentlyPlayingReducer: Reducer<
    CurrentlyPayingState,
    ClickedPlayAction
> = (
    state: CurrentlyPayingState = defaultState,
    { type, payload }: ClickedPlayAction
) => {
    switch (type) {
        case CLICKED_PLAY_BUTTON:
            state = {
                ...state,
                ...payload,
            };
            break;
        default:
            return state;
    }
    return state;
};

export default currentlyPlayingReducer;
