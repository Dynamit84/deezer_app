import { Reducer } from "redux";
import { FETCH_ALBUM_SUCCESS, FETCH_ALBUM_FAILS } from "../actions/actionTypes";
import { ERROR_MSG } from "../constants/texts";
import { Album, Artist } from "../types";
import { FetchDataActions } from "../actions/types";

const defaultState: Album = {
    isLoading: true,
    cover_big: null,
    cover_medium: null,
    artist: {} as Artist,
    fans: null,
    release_date: null,
    id: null,
    title: null,
};
const albumReducer: Reducer<Album, FetchDataActions> = (
    state: Album = defaultState,
    { type, payload }: FetchDataActions
) => {
    switch (type) {
        case FETCH_ALBUM_SUCCESS:
            state = {
                ...state,
                ...payload,
                isLoading: false,
            };
            break;
        case FETCH_ALBUM_FAILS:
            state = {
                ...state,
                errorMsg: ERROR_MSG,
                isLoading: false,
            };
            break;
        default:
            return state;
    }
    return state;
};

export default albumReducer;
