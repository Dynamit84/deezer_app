import { Reducer } from "redux";
import { SearchState } from "./types";
import {
    FETCH_SEARCH_ALBUMS,
    FETCH_SEARCH_ALBUMS_FAILS,
    FETCH_SEARCH_ALBUMS_SUCCESS,
    FETCH_SEARCH_ARTISTS,
    FETCH_SEARCH_ARTISTS_FAILS,
    FETCH_SEARCH_ARTISTS_SUCCESS,
    FETCH_SEARCH_PLAYLISTS,
    FETCH_SEARCH_PLAYLISTS_FAILS,
    FETCH_SEARCH_PLAYLISTS_SUCCESS,
    FETCH_SEARCH_TRACKS,
    FETCH_SEARCH_TRACKS_FAILS,
    FETCH_SEARCH_TRACKS_SUCCESS,
    FETCH_SEARCH_DATA_IN_PROGRESS,
    FETCH_SEARCH_DATA_FAILS,
    FETCH_SEARCH_DATA_SUCCESS,
} from "../actions/actionTypes";

import { ERROR_MSG } from "../constants/texts";

const regex = /q=(.*)/gi;

const defaultState: SearchState = {
    tracks: {
        data: [],
        isLoading: false,
    },
    artists: {
        data: [],
        isLoading: false,
    },
    playlists: {
        data: [],
        isLoading: false,
    },
    albums: {
        data: [],
        isLoading: false,
    },
    isAllDataLoading: false,
};
const searchReducer: Reducer<SearchState> = (
    state: SearchState = defaultState,
    { type, payload }
) => {
    switch (type) {
        case FETCH_SEARCH_ALBUMS_SUCCESS:
            state = {
                ...state,
                albums: {
                    data: [...state.albums.data, ...payload.data],
                    next: payload.next && payload.next.match(regex)[0],
                    isLoading: false,
                    total: payload.total,
                },
            };
            break;
        case FETCH_SEARCH_ALBUMS:
            state = {
                ...state,
                albums: {
                    ...state.albums,
                    isLoading: true,
                },
            };
            break;
        case FETCH_SEARCH_ALBUMS_FAILS:
            state = {
                ...state,
                errorMsg: ERROR_MSG,
            };
            break;
        case FETCH_SEARCH_PLAYLISTS_SUCCESS:
            state = {
                ...state,
                playlists: {
                    data: [...state.playlists.data, ...payload.data],
                    next: payload.next && payload.next.match(regex)[0],
                    isLoading: false,
                    total: payload.total,
                },
            };
            break;
        case FETCH_SEARCH_PLAYLISTS:
            state = {
                ...state,
                playlists: {
                    ...state.playlists,
                    isLoading: true,
                },
            };
            break;
        case FETCH_SEARCH_PLAYLISTS_FAILS:
            state = {
                ...state,
                errorMsg: ERROR_MSG,
            };
            break;
        case FETCH_SEARCH_TRACKS_SUCCESS:
            state = {
                ...state,
                tracks: {
                    data: [...state.tracks.data, ...payload.data],
                    next: payload.next && payload.next.match(regex)[0],
                    isLoading: false,
                    total: payload.total,
                },
            };
            break;
        case FETCH_SEARCH_TRACKS:
            state = {
                ...state,
                tracks: {
                    ...state.tracks,
                    isLoading: true,
                },
            };
            break;
        case FETCH_SEARCH_TRACKS_FAILS:
            state = {
                ...state,
                errorMsg: ERROR_MSG,
            };
            break;
        case FETCH_SEARCH_ARTISTS_SUCCESS:
            state = {
                ...state,
                artists: {
                    data: [...state.artists.data, ...payload.data],
                    next: payload.next && payload.next.match(regex)[0],
                    isLoading: false,
                    total: payload.total,
                },
            };
            break;
        case FETCH_SEARCH_ARTISTS:
            state = {
                ...state,
                artists: {
                    ...state.artists,
                    isLoading: true,
                },
            };
            break;
        case FETCH_SEARCH_ARTISTS_FAILS:
            state = {
                ...state,
                errorMsg: ERROR_MSG,
            };
            break;
        case FETCH_SEARCH_DATA_SUCCESS:
            state = {
                ...state,
                isAllDataLoading: false,
            };
            break;
        case FETCH_SEARCH_DATA_IN_PROGRESS:
            state = {
                ...state,
                isAllDataLoading: true,
            };
            break;
        case FETCH_SEARCH_DATA_FAILS:
            state = {
                ...state,
                isAllDataLoading: false,
            };
            break;
        default:
            return state;
    }
    return state;
};

export default searchReducer;
