import { Reducer } from "redux";
import {
    FETCH_GENRES_SUCCESS,
    FETCH_GENRES_FAILS,
    CHANGE_GENRE,
    FETCH_GENRES,
} from "../actions/actionTypes";
import { ERROR_MSG } from "../constants/texts";
import { GenresState } from "./types";
import { GenresActions } from "../actions/types";

const defaultState: GenresState = {
    musicGenres: [],
    currentGenre: {
        name: "All",
        id: 0,
    },
    isLoading: false,
};
const genresReducer: Reducer<GenresState, GenresActions> = (
    state: GenresState = defaultState,
    { type, payload }: GenresActions
) => {
    switch (type) {
        case FETCH_GENRES_SUCCESS:
            state = {
                ...state,
                musicGenres: payload.data,
                isLoading: false,
            };
            break;
        case FETCH_GENRES_FAILS:
            state = {
                ...state,
                isLoading: false,
                errorMsg: ERROR_MSG,
            };
            break;
        case FETCH_GENRES:
            state = {
                ...state,
                isLoading: true,
            };
            break;
        case CHANGE_GENRE:
            state = {
                ...state,
                currentGenre: payload,
            };
            break;
        default:
            return state;
    }
    return state;
};

export default genresReducer;
