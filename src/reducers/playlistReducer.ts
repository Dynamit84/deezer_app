import { Reducer } from "redux";
import {
    FETCH_PLAYLIST_SUCCESS,
    FETCH_PLAYLIST_FAILS,
} from "../actions/actionTypes";
import { ERROR_MSG } from "../constants/texts";
import { PlaylistState } from "../reducers/types";

const defaultState: PlaylistState = { tracks: { data: [] }, isLoading: true };
const playlistReducer: Reducer<PlaylistState> = (
    state: PlaylistState = defaultState,
    { type, payload }
) => {
    switch (type) {
        case FETCH_PLAYLIST_SUCCESS:
            state = {
                ...payload,
                isLoading: false,
            };
            break;
        case FETCH_PLAYLIST_FAILS:
            state = {
                ...defaultState,
                errorMsg: ERROR_MSG,
                isLoading: false,
            };
            break;
        default:
            return state;
    }
    return state;
};

export default playlistReducer;
