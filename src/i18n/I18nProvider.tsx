import React, { useState, useEffect, FC, ReactNode } from "react";
import { connect } from "react-redux";
import { IntlProvider } from "react-intl";
import getMessages from "../helpers/getMessages";
import { ApplicationState } from "../reducers/types";

type MapStateToProps = {
    language: string;
};

type PassedProps = {
    children: ReactNode;
};

type Props = MapStateToProps & PassedProps;

const I18nProvider: FC<Props> = ({ language, children }) => {
    const [messages, setMessages] = useState<Record<string, string> | null>(
        null
    );
    useEffect(() => {
        getMessages(language).then((messages) => setMessages(messages));
    }, [language]);
    return (
        messages && (
            <IntlProvider locale={language} messages={messages}>
                {children}
            </IntlProvider>
        )
    );
};

const mapStateToProps = ({
    user: { language },
}: ApplicationState): MapStateToProps => ({
    language,
});

export default connect(mapStateToProps)(I18nProvider);
