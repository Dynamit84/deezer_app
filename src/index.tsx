import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import App from "./App";
import { Provider } from "react-redux";
import store from "./store";
import { BrowserRouter as Router } from "react-router-dom";
import { registerObserver } from "react-perf-devtool";
import I18nProvider from "./i18n/I18nProvider";
import AppThemeProvider from "./components/ThemeProvider";
import { StylesProvider } from "@material-ui/core/styles";

registerObserver();
ReactDOM.render(
    <Provider store={store}>
        <Router>
            <I18nProvider>
                <StylesProvider injectFirst>
                    <AppThemeProvider>
                        <App />
                    </AppThemeProvider>
                </StylesProvider>
            </I18nProvider>
        </Router>
    </Provider>,
    document.getElementById("root")
);
