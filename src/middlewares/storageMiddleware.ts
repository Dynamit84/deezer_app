import localStorage from "../helpers/localStorage";
import { UPDATE_LOCAL_STORAGE } from "../actions/actionTypes";
import { MiddlewareAPI, Dispatch, Middleware, AnyAction } from "redux";

const storageMiddleware: Middleware = (store: MiddlewareAPI) => (
    next: Dispatch<AnyAction>
) => (action: AnyAction) => {
    switch (action.type) {
        case UPDATE_LOCAL_STORAGE:
            localStorage.saveState(action.payload);
            break;
        default:
            break;
    }
    next(action);
};

export default storageMiddleware;
