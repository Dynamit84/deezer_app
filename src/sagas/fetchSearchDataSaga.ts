import { put, all } from "redux-saga/effects";
import {
    fetchDataSuccess,
    fetchDataFails,
    fetchingData,
} from "../actions/actions";
import { SEARCH_DATA } from "../constants/texts";
import { fetchData } from "./fetchDataSaga";

export function* fetchSearchData(action: any) {
    try {
        yield put(fetchingData(SEARCH_DATA));
        const callFetchData = action.payload.map((fetchAction: any) => {
            return fetchData(fetchAction);
        });
        yield all(callFetchData);
        yield put(fetchDataSuccess(SEARCH_DATA));
    } catch (error) {
        yield put(fetchDataFails(SEARCH_DATA, error));
    }
}
