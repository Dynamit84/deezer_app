import { fetchData } from "./fetchDataSaga";
import { fetchSearchData } from "./fetchSearchDataSaga";
import { FETCH_DATA, FETCH_SEARCH_DATA } from "../actions/actionTypes";
import { takeEvery } from "redux-saga/effects";

export function* watchfetchData() {
    yield takeEvery(FETCH_DATA, fetchData);
}

export function* watchSearchSaga() {
    yield takeEvery(FETCH_SEARCH_DATA, fetchSearchData);
}
