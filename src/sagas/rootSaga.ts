import { all } from "redux-saga/effects";
import { watchfetchData, watchSearchSaga } from "./watchSagas";

export default function* rootSaga() {
    yield all([watchfetchData(), watchSearchSaga()]);
}
