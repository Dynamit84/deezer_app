import { call, put } from "redux-saga/effects";
import fetchAsyncData from "../api";
import {
    fetchDataSuccess,
    fetchDataFails,
    fetchingData,
} from "../actions/actions";

export function* fetchData(action: any) {
    try {
        yield put(fetchingData(action.payload.entity));
        const data = yield call(fetchAsyncData, action.payload.url);
        if (data.error) {
            yield put(fetchDataFails(action.payload.entity, data.error));
        } else {
            yield put(fetchDataSuccess(action.payload.entity, data));
        }
    } catch (error) {
        yield put(fetchDataFails(action.payload.entity, error));
    }
}
