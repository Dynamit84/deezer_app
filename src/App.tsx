import React, { useEffect } from "react";
import "./app.scss";
import { Switch, Route } from "react-router-dom";
import AppBar from "./components/AppBar";
import Home from "./components/pages/HomePage/styled/StyledHomePage";
import ArtistPage from "./components/pages/ArtistPage";
import AlbumPage from "./components/pages/AlbumPage";
import PlaylistPage from "./components/pages/PlaylistPage";
import ProfilePage from "./components/pages/ProfilePage";
import SearchPage from "./components/pages/SearchPage";
import Buttons from "./components/common/Buttons";
import styled from "styled-components/macro";
import { connect } from "react-redux";
import { playerLoaded } from "./actions/actions";

const StyledApp = styled(App)`
    padding-bottom: 92px;
    background-color: ${(props) => props.theme.palette.background.default};
    color: ${(props) => props.theme.palette.text.primary};
`;

type Props = {
     className?: string;
     playerLoaded: ()=> void;
}

function App({ className, playerLoaded }: Props  ): JSX.Element {
    const { ScrollToTopButton } = Buttons;
    useEffect(() => {
        const intervalId = setInterval(() => {
            if(window.DZ) {
                window.DZ.ready(function () {
                    console.log("DZ SDK is ready");
                    playerLoaded();
                });
                clearInterval(intervalId);
            }
        }, 300);
            return () => clearInterval(intervalId);
    }, [playerLoaded]);
    return (
        <div className={`app ${className}`}>
            <AppBar />
            <Switch>
                <Route exact path="/">
                    <Home />
                </Route>
                <Route path="/artist/:id">
                    <ArtistPage />
                </Route>
                <Route path="/album/:id">
                    <AlbumPage />
                </Route>
                <Route path="/playlist/:id">
                    <PlaylistPage />
                </Route>
                <Route path="/user/:id">
                    <ProfilePage />
                </Route>
                <Route path="/search">
                    <SearchPage />
                </Route>
            </Switch>
            <ScrollToTopButton />
        </div>
    );
}

export default connect(null, {playerLoaded})(StyledApp);
