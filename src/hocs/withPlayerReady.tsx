import React, { useEffect, ComponentType } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { playerLoaded } from "../actions/actions";

type Props = {
    playerLoaded: Function;
};

const withPlayerReady = <P extends object>(
    WrappedComponent: ComponentType<P>
) => (props: Props) => {
    const { playerLoaded } = props;
    useEffect(() => {
        const intervalId = setInterval(() => {
            if (window.DZ && window.DZ.player && window.DZ.player.loaded) {
                playerLoaded();
                clearInterval(intervalId);
            }
        }, 300);
        return () => clearInterval(intervalId);
    }, [playerLoaded]);

    return <WrappedComponent {...(props as P)} />;
};

const composedWithPlayerReady = compose(
    connect(null, { playerLoaded }),
    withPlayerReady
);

export default composedWithPlayerReady;
