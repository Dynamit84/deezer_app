import React, { ComponentType } from "react";
import { Loader } from "../components/common/Loader";

type LoaderProps = {
    isLoading: boolean;
};

export default function withLoader<Props>(
    WrappedComponent: ComponentType<Props>
) {
    return (props: Props & LoaderProps) => {
        const { isLoading, ...restProps } = props as LoaderProps;
        return isLoading ? (
            <Loader />
        ) : (
            <WrappedComponent {...(restProps as Props)} />
        );
    };
}
