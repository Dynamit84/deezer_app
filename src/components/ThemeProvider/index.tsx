import React, { FC, ReactNode } from "react";
import { connect } from "react-redux";
import { createMuiTheme } from "@material-ui/core/styles";
import { PaletteType } from "@material-ui/core";
import { ApplicationState } from "../../reducers/types";
import grey from "@material-ui/core/colors/grey";
import { ThemeProvider } from "styled-components";

type MapStateToProps = {
    theme: PaletteType;
};

type PassedProps = {
    children: ReactNode;
};

type Props = MapStateToProps & PassedProps;

const AppThemeProvider: FC<Props> = ({ theme: userTheme, children }) => {
    const userThemes = {
        dark: {
            palette: {
                type: userTheme,
                background: {
                    default: grey[900],
                },
                text: {
                    primary: "#fff",
                },
                primary: {
                    main: "#333",
                    light: "#616161",
                },
                secondary: {
                    main: "#b1b1b1",
                    light: "#424242",
                },
            },
        },
        light: {
            palette: {
                type: userTheme,
                text: {
                    primary: "#000",
                },
                primary: {
                    main: "#1976d2",
                },
                secondary: {
                    main: "#eaeaea",
                },
            },
        },
    };
    const theme = createMuiTheme(userThemes[userTheme]);
    return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};

const mapStateToProps = ({
    user: { theme },
}: ApplicationState): MapStateToProps => ({
    theme,
});

export default connect(mapStateToProps)(AppThemeProvider);
