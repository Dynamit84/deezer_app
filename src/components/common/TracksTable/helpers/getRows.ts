import { Track } from "../../../../types";
import { convertDuration } from "../../../../helpers/formatData";

export type Row = {
    id: number;
    number?: number;
    track?: string;
    artist?: string;
    album?: string | null;
    length?: string;
};

export type Columns = string[];

export default (tracks: Track[], columns: Columns) => {
    return tracks.map(
        ({ title, artist, album, duration, id }, index: number) => {
            let row: Row = { id };
            columns.forEach((column) => {
                switch (column) {
                    case "number":
                        row.number = ++index;
                        break;
                    case "track":
                        row.track = title;
                        break;
                    case "artist":
                        row.artist = artist.name;
                        break;
                    case "album":
                        row.album = album.title;
                        break;
                    case "length":
                        row.length = convertDuration(duration);
                        break;

                    default:
                        break;
                }
            });
            return row;
        }
    );
};
