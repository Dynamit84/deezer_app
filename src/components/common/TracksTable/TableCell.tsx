import React from "react";
import PlayButton from "../Buttons/PlayButton";
import { TableCell } from "@material-ui/core";
import { Row } from "./helpers/getRows";

type Props = {
    className: string;
    labelId: string;
    row: Row;
    handleClick: (id: number) => void;
    isPlaying: boolean;
    isVisible: boolean;
};

const EnhancedTableCell: React.FC<Props> = ({
    className,
    labelId,
    row,
    handleClick,
    isPlaying,
    isVisible,
}) => {
    return (
        <TableCell
            component="th"
            id={labelId}
            scope="row"
            className={isVisible ? `${className} button-visible` : className}
            align="center"
        >
            {row.number}
            <PlayButton
                onClick={handleClick.bind(null, row.id)}
                isPlaying={isPlaying}
                isVisible={isVisible}
            />
        </TableCell>
    );
};

export default EnhancedTableCell;
