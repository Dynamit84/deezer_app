import styled from "styled-components/macro";
import { TableBody } from "@material-ui/core";

const StyledTableBody = styled(TableBody)`
    background-color: ${({ theme: { palette } }) =>
        palette.type === "light"
            ? palette.common.white
            : palette.secondary.light};

    .MuiTableCell-root {
        color: ${({ theme: { palette } }) =>
            palette.type === "light"
                ? "rgba(0, 0, 0, 0.87)"
                : palette.common.white};
    }
    .MuiTableRow-root {
        &:hover {
            background-color: ${({ theme: { palette } }) =>
                palette.type === "light"
                    ? "rgba(0, 0, 0, 0.04);"
                    : "rgba(255, 255, 255, 0.08)"};
        }
    }
`;

export default StyledTableBody;
