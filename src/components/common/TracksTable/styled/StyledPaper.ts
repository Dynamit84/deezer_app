import styled from "styled-components/macro";
import Paper from "@material-ui/core/Paper";

const StyledPaper = styled(Paper)`
    width: 100%;
    margin-bottom: ${(props) => props.theme.spacing(2)}px;
    .table {
        min-width: 750px;
    }
    .table-wrapper {
        overflow-x: auto;
    }
    .visually-hidden {
        border: 0;
        clip: rect(0 0 0 0);
        height: 1px;
        margin: -1px;
        overflow: hidden;
        padding: 0;
        position: absolute;
        top: 20px;
        width: 1px;
    }
    .number {
        width: 10px;
    }
    .length {
        width: 15px;
    }
    .MuiTableCell-root {
        border-color: ${({ theme: { palette } }) =>
            palette.type === "light"
                ? "rgba(224, 224, 224, 1)"
                : "rgba(81, 81, 81, 1)"};
    }
    .MuiTableBody-root {
        .MuiTableRow-root {
            .MuiTableCell-root {
                position: relative;
                .MuiIconButton-root {
                    display: none;
                    transform: none;
                    color: ${({ theme: { palette } }) =>
                        palette.type === "light"
                            ? palette.common.white
                            : palette.common.black};
                    background-color: ${({ theme: { palette } }) =>
                        palette.type === "light"
                            ? palette.common.black
                            : palette.common.white};
                    width: 30px;
                    height: 30px;
                    left: 21px;
                    top: calc(50% - 15px);

                    .arrow-icon {
                        width: 20px;
                        height: 20px;
                    }
                }

                &.button-visible {
                    .MuiIconButton-root {
                        display: inline-block;
                    }
                }
            }

            &:hover {
                .MuiTableCell-root {
                    .MuiIconButton-root {
                        display: inline-block;
                    }
                }
            }
        }
    }
`;

export default StyledPaper;
