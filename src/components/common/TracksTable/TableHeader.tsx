import React, { MouseEvent } from "react";
import {
    TableCell,
    TableHead,
    TableRow,
    TableSortLabel,
} from "@material-ui/core";
import { Sort } from ".";
import { Columns } from "./helpers/getRows";
import { useIntl } from "react-intl";
import styled from "styled-components/macro";

const defaultHeadCells = [
    {
        value: "number",
        id: "app.number",
        numeric: true,
        disablePadding: true,
    },
    {
        value: "track",
        id: "app.track",
        numeric: false,
        disablePadding: false,
    },
    {
        value: "artist",
        id: "app.artist",
        numeric: false,
        disablePadding: false,
    },
    {
        value: "album",
        id: "app.album",
        numeric: false,
        disablePadding: false,
    },
    {
        value: "length",
        numeric: false,
        disablePadding: false,
        id: "app.length",
    },
];

type HeadCells = typeof defaultHeadCells;

const getHeadCells = (columns: Columns): HeadCells => {
    return defaultHeadCells.filter(
        (cell) => columns.includes(cell.value) && cell
    );
};

type Props = {
    order: Sort;
    orderBy: string;
    onRequestSort: (
        event: MouseEvent<HTMLAnchorElement, globalThis.MouseEvent>,
        property: string
    ) => void;
    columns: Columns;
};

const StyledTableCell = styled(({ ...rest }) => (
    <TableCell classes={{ root: "header-cell" }} {...rest} />
))`
    &.header-cell {
        background-color: ${({ theme: { palette } }) =>
            palette.type === "light" ? "#fafafa" : palette.primary.main};
        color: ${({ theme: { palette } }) =>
            palette.type === "light"
                ? palette.common.black
                : palette.common.white};
    }
    .MuiTableSortLabel-root {
        &:hover {
            color: ${({ theme: { palette } }) =>
                palette.type === "light"
                    ? "rgba(0, 0, 0, 0.54)"
                    : "rgba(255, 255, 255, 0.7)"};
        }
    }
`;

const EnhancedTableHead: React.FC<Props> = (props) => {
    const { order, orderBy, onRequestSort, columns } = props;
    const { formatMessage } = useIntl();
    const createSortHandler = (property: string) => (
        event: MouseEvent<HTMLAnchorElement, globalThis.MouseEvent>
    ) => {
        onRequestSort(event, property);
    };
    const headCells = getHeadCells(columns);

    return (
        <TableHead>
            <TableRow>
                {headCells.map((headCell) => (
                    <StyledTableCell
                        key={headCell.value}
                        sortDirection={
                            orderBy === headCell.value ? order : false
                        }
                        align={headCell.value === "number" ? "center" : "left"}
                    >
                        <TableSortLabel
                            active={orderBy === headCell.value}
                            direction={order}
                            onClick={createSortHandler(headCell.value)}
                        >
                            {formatMessage({ id: headCell.id })}
                            {orderBy === headCell.value ? (
                                <span className="visually-hidden">
                                    {order === Sort.desc
                                        ? "sorted descending"
                                        : "sorted ascending"}
                                </span>
                            ) : null}
                        </TableSortLabel>
                    </StyledTableCell>
                ))}
            </TableRow>
        </TableHead>
    );
};

export default EnhancedTableHead;
