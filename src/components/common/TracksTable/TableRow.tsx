import React, { useEffect, FC } from "react";
import EnhancedTableCell from "./TableCell";
import { TableCell, TableRow } from "@material-ui/core";
import { connect } from "react-redux";
import { clickedPlay } from "../../../actions/actions";
import { ItemInfo } from "../../../actions/types";
import { ApplicationState } from "../../../reducers/types";
import { Row } from "./helpers/getRows";

type PassedProps = {
    labelId: string;
    row: Row;
    entityId?: number | null;
};

type MapStateToProps = {
    trackId: number;
    isPlaying: boolean;
};

type MapDispatchToprops = {
    clickedPlay: ({
        trackId,
        type,
        entityId,
        isPlaying,
    }: Partial<ItemInfo>) => void;
};

type Props = MapStateToProps & MapDispatchToprops & PassedProps;

const EnhancedTableRow: FC<Props> = ({
    labelId,
    row,
    clickedPlay,
    trackId,
    entityId,
    isPlaying,
}) => {
    const handleDoubleClick = (id: number) => {
        handleClick(id);
    };

    const handleClick = (id: number) => {
        //if(isPlayerLoaded) {
        if (trackId === id) {
            if (isPlaying) {
                clickedPlay({ isPlaying: false });
                window.DZ.player.pause();
            } else {
                clickedPlay({ isPlaying: true });
                window.DZ.player.play();
            }
        } else {
            clickedPlay({ trackId: id, entityId, isPlaying: true });
            window.DZ.player.playTracks([id]);
        }
        //}
    };

    return (
        <TableRow onDoubleClick={handleDoubleClick.bind(null, row.id)} hover>
            {row.number && (
                <EnhancedTableCell
                    className="number"
                    labelId={labelId}
                    row={row}
                    handleClick={handleClick}
                    isPlaying={isPlaying}
                    isVisible={trackId === row.id}
                />
            )}
            {row.track && <TableCell>{row.track}</TableCell>}
            {row.artist && <TableCell>{row.artist}</TableCell>}
            {row.album && <TableCell>{row.album}</TableCell>}
            {row.length && (
                <TableCell className="length">{row.length}</TableCell>
            )}
        </TableRow>
    );
};

const mapStateToProps = ({
    currentlyPlaying: { trackId, isPlaying },
}: ApplicationState): MapStateToProps => ({
    trackId,
    isPlaying,
});

export default connect(mapStateToProps, { clickedPlay })(EnhancedTableRow);
