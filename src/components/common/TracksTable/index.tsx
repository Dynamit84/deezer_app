import React, { useRef, useState, useCallback, MouseEvent } from "react";
import { Table } from "@material-ui/core";
import Paper from "./styled/StyledPaper";
import EnhancedTableHead from "./TableHeader";
import EnhancedTableRow from "./TableRow";
import { stableSort, getSorting } from "../../../helpers/formatData";
import { Loader } from "../Loader";
import { DataForFetching } from "../../../types";
import { GeneralData, Track } from "../../../types";
import getRows, { Columns, Row } from "./helpers/getRows";
import TableBody from "./styled/StyledTableBody";

export enum Sort {
    asc = "asc",
    desc = "desc",
}

type Props = {
    columns: Columns;
    tracks: GeneralData;
    dataForFetching?: DataForFetching;
    entityId?: number | null;
};

export default ({
    tracks: { data, isLoading, next },
    columns,
    dataForFetching = {} as DataForFetching,
    entityId,
}: Props): JSX.Element => {
    const { id, entity, fetchData } = dataForFetching;
    const [order, setOrder] = useState<Sort>(Sort.asc);
    const hasMore = Boolean(next);
    const [orderBy, setOrderBy] = useState<string>("tracks");

    const handleRequestSort = (
        event: MouseEvent<HTMLAnchorElement, globalThis.MouseEvent>,
        property: string
    ) => {
        const isDesc = orderBy === property && order === Sort.desc;
        setOrder(isDesc ? Sort.asc : Sort.desc);
        setOrderBy(property);
    };

    const rows = getRows(data as Track[], columns);

    const observer = useRef<IntersectionObserver | null>(null);
    const lastTrackElementRef = useCallback(
        (node) => {
            if (isLoading) return;
            if (observer.current) {
                observer.current.disconnect();
            }

            observer.current = new IntersectionObserver(
                (entries) => {
                    if (entries[0].isIntersecting && hasMore) {
                        fetchData && fetchData(entity, { id, params: next });
                    }
                },
                { threshold: 1 }
            );
            if (node) {
                observer.current.observe(node);
            }
        },
        [isLoading, hasMore, next, fetchData, entity, id]
    );

    return (
        <>
            <Paper>
                <div className="table-wrapper">
                    <Table
                        className="table"
                        aria-labelledby="tableTitle"
                        aria-label="enhanced table"
                        stickyHeader
                    >
                        <EnhancedTableHead
                            order={order}
                            orderBy={orderBy}
                            onRequestSort={handleRequestSort}
                            columns={columns}
                        />
                        <TableBody>
                            {stableSort(rows, getSorting(order, orderBy)).map(
                                (row: Row, index) => {
                                    const labelId = `enhanced-table-checkbox-${index}`;
                                    return (
                                        <EnhancedTableRow
                                            row={row}
                                            labelId={labelId}
                                            key={row.id}
                                            entityId={entityId}
                                        />
                                    );
                                }
                            )}
                            {!isLoading && hasMore && (
                                <tr ref={lastTrackElementRef} />
                            )}
                        </TableBody>
                    </Table>
                </div>
            </Paper>
            {isLoading && (
                <div className="loader-container small">
                    <Loader />
                </div>
            )}
        </>
    );
};
