import React, { useState, FC } from "react";
import Tabs from "./Tabs";
import TabPanels from "./TabPanels";
import {
    getPlaylistUrl,
    getAlbumUrl,
    getUserUrl,
    getArtistInfoUrl,
} from "../../../helpers/getUrls";
import {
    DISCOGRAPHY,
    COMMENTS,
    SIMILAR,
    PLAYLISTS,
    MOSTPLAYED,
    FOLLOWERS,
    ALBUMS,
    FOLLOWING,
    USER_PLAYLISTS,
    USER_ARTISTS,
    SEARCH_ARTISTS,
    SEARCH_TRACKS,
    SEARCH_ALBUMS,
    SEARCH_PLAYLISTS,
} from "../../../constants/texts";
import TracksContentTab from "./TabPanelsContent/TracksContentTab";
import CardsContentTab from "./TabPanelsContent/CardsContentTab";
import { Tab } from "./Tabs";
import { FetchData, GeneralData } from "../../../types";
import { SearchState, ProfileState } from "../../../reducers/types";
import AppBar from "./styled/StyledAppBar";

export type CurrentView = {
    discography: GeneralData;
    mostPlayed: GeneralData;
    similar: GeneralData;
    playlists: GeneralData;
    comments: GeneralData;
    artists: GeneralData;
    albums: GeneralData;
    followings: GeneralData;
    followers: GeneralData;
    tracks: GeneralData;
    search: SearchState;
    profile: ProfileState;
};

type Props = {
    id?: number;
    fetchData: FetchData;
    pageTabs: string[];
    currentView: Partial<CurrentView>;
    currentTab?: string;
};

const NavBar: FC<Props> = ({
    id,
    fetchData,
    pageTabs,
    currentView,
    currentTab,
}) => {
    const dataForFetching = { id, fetchData };
    const tabs = [
        {
            caption: DISCOGRAPHY,
            tabPanel: (
                <CardsContentTab
                    tabData={currentView.discography as GeneralData}
                    linkUrl={getAlbumUrl}
                    dataForFetching={{
                        ...dataForFetching,
                        entity: DISCOGRAPHY,
                    }}
                />
            ),
        },
        {
            caption: MOSTPLAYED,
            tabPanel: (
                <TracksContentTab
                    tabData={currentView.mostPlayed as GeneralData}
                    dataForFetching={{
                        ...dataForFetching,
                        entity: MOSTPLAYED,
                    }}
                />
            ),
        },
        {
            caption: SIMILAR,
            tabPanel: (
                <CardsContentTab
                    tabData={currentView.similar as GeneralData}
                    linkUrl={getArtistInfoUrl}
                    dataForFetching={{
                        ...dataForFetching,
                        entity: SIMILAR,
                    }}
                />
            ),
        },
        {
            caption: PLAYLISTS,
            tabPanel: (
                <CardsContentTab
                    tabData={currentView.playlists as GeneralData}
                    linkUrl={getPlaylistUrl}
                    dataForFetching={{
                        ...dataForFetching,
                        entity: PLAYLISTS,
                    }}
                />
            ),
        },
        {
            caption: COMMENTS,
            tabPanel: (
                <CardsContentTab
                    tabData={currentView.comments as GeneralData}
                    isComments
                    dataForFetching={{
                        ...dataForFetching,
                        entity: COMMENTS,
                    }}
                />
            ),
        },
        {
            caption: USER_PLAYLISTS,
            tabPanel: (
                <CardsContentTab
                    withNumber
                    tabData={currentView.playlists as GeneralData}
                    linkUrl={getPlaylistUrl}
                    dataForFetching={{
                        ...dataForFetching,
                        entity: USER_PLAYLISTS,
                    }}
                />
            ),
        },
        {
            caption: USER_ARTISTS,
            tabPanel: (
                <CardsContentTab
                    withNumber
                    tabData={currentView.artists as GeneralData}
                    linkUrl={getArtistInfoUrl}
                    dataForFetching={{
                        ...dataForFetching,
                        entity: USER_ARTISTS,
                    }}
                />
            ),
        },
        {
            caption: ALBUMS,
            tabPanel: (
                <CardsContentTab
                    withNumber
                    tabData={currentView.albums as GeneralData}
                    linkUrl={getAlbumUrl}
                    dataForFetching={{
                        ...dataForFetching,
                        entity: ALBUMS,
                    }}
                />
            ),
        },
        {
            caption: FOLLOWING,
            tabPanel: (
                <CardsContentTab
                    withNumber
                    tabData={currentView.followings as GeneralData}
                    linkUrl={getUserUrl}
                    dataForFetching={{
                        ...dataForFetching,
                        entity: FOLLOWING,
                    }}
                />
            ),
        },
        {
            caption: FOLLOWERS,
            tabPanel: (
                <CardsContentTab
                    withNumber
                    tabData={currentView.followers as GeneralData}
                    linkUrl={getUserUrl}
                    dataForFetching={{
                        ...dataForFetching,
                        entity: FOLLOWERS,
                    }}
                />
            ),
        },
        {
            caption: SEARCH_PLAYLISTS,
            tabPanel: (
                <CardsContentTab
                    withNumber
                    tabData={currentView.playlists as GeneralData}
                    linkUrl={getPlaylistUrl}
                    dataForFetching={{
                        ...dataForFetching,
                        entity: SEARCH_PLAYLISTS,
                    }}
                />
            ),
        },
        {
            caption: SEARCH_TRACKS,
            tabPanel: (
                <TracksContentTab
                    withNumber
                    tabData={currentView.tracks as GeneralData}
                    dataForFetching={{
                        ...dataForFetching,
                        entity: SEARCH_TRACKS,
                    }}
                />
            ),
        },
        {
            caption: SEARCH_ALBUMS,
            tabPanel: (
                <CardsContentTab
                    withNumber
                    tabData={currentView.albums as GeneralData}
                    linkUrl={getAlbumUrl}
                    dataForFetching={{
                        ...dataForFetching,
                        entity: SEARCH_ALBUMS,
                    }}
                />
            ),
        },
        {
            caption: SEARCH_ARTISTS,
            tabPanel: (
                <CardsContentTab
                    withNumber
                    tabData={currentView.artists as GeneralData}
                    linkUrl={getArtistInfoUrl}
                    dataForFetching={{
                        ...dataForFetching,
                        entity: SEARCH_ARTISTS,
                    }}
                />
            ),
        },
    ];

    const getTabsSettings = () =>
        tabs.filter((tab: Tab) => pageTabs.includes(tab.caption));
    const tabsSettings = getTabsSettings();
    const [value, setValue] = useState<string>(
        currentTab || tabsSettings[0].caption
    );

    const handleChange = (event: React.ChangeEvent<{}>, newValue: string) => {
        setValue(newValue);
    };
    return (
        <div className="catalog-container">
            <AppBar position="static" color="default">
                <Tabs
                    value={value}
                    handleChange={handleChange}
                    tabs={tabsSettings}
                />
            </AppBar>
            <TabPanels tabs={tabsSettings} value={value} />
        </div>
    );
};

export default NavBar;
