import React, { useEffect, FC } from "react";
import TracksTable from "../../TracksTable";
import TabCaption from "../../TabCaption";
import { DataForFetching, GeneralData } from "../../../../types";
import { useIntl } from "react-intl";
import { getText } from "../../../../helpers/getText";

type Props = {
    dataForFetching: DataForFetching;
    tabData: GeneralData;
    withNumber?: boolean;
};

const tableColumns = ["number", "track", "artist", "album", "length"];

const TracksContentTab: FC<Props> = ({
    tabData,
    dataForFetching,
    dataForFetching: { id, entity, fetchData },
    withNumber,
}) => {
    useEffect(() => {
        if (tabData.isFirstLoad) fetchData(entity, { id });
    }, [fetchData, entity, id, tabData.isFirstLoad]);

    const { formatMessage } = useIntl();
    const tabText = formatMessage({
        id: getText(entity),
    });
    return (
        <>
            <TabCaption
                withNumber={withNumber}
                caption={tabText}
                total={tabData.total}
            />
            <TracksTable
                tracks={tabData}
                columns={tableColumns}
                dataForFetching={dataForFetching}
            />
        </>
    );
};

export default TracksContentTab;
