import React, { useEffect, FC } from "react";
import CardsList from "../../CardsList";
import TabCaption from "../../TabCaption";
import CommentsList from "../../CommentsList";
import ErrorBoundary from "../../ErrorBoundary";
import { FOLLOWERS, FOLLOWING } from "../../../../constants/texts";
import { DataForFetching, GeneralData, LinkUrl } from "../../../../types";
import { getText } from "../../../../helpers/getText";
import { useIntl } from "react-intl";

type Props = {
    isComments?: boolean;
    linkUrl?: LinkUrl;
    withNumber?: boolean;
    tabData: GeneralData;
    dataForFetching: DataForFetching;
};

const CardsContentTab: FC<Props> = ({
    tabData,
    withNumber,
    isComments,
    linkUrl,
    dataForFetching,
    dataForFetching: { id, entity, fetchData },
}) => {
    const { formatMessage } = useIntl();
    const tabText = formatMessage({
        id: getText(entity),
    });

    useEffect(() => {
        const excludeFetch = [FOLLOWERS, FOLLOWING];
        if (excludeFetch.includes(entity)) return;
        tabData.isFirstLoad && fetchData(entity, { id });
    }, [fetchData, entity, id, tabData.isFirstLoad]);

    return (
        <ErrorBoundary>
            <TabCaption
                withNumber={withNumber}
                caption={tabText}
                total={tabData.total}
            />
            {isComments ? (
                <CommentsList
                    data={tabData}
                    isLoading={tabData.isLoading}
                    dataForFetching={dataForFetching}
                />
            ) : (
                <CardsList
                    data={tabData}
                    linkUrl={linkUrl as LinkUrl}
                    dataForFetching={dataForFetching}
                />
            )}
        </ErrorBoundary>
    );
};

export default CardsContentTab;
