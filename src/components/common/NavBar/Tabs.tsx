import React from "react";
import { Tabs, Tab } from "@material-ui/core";
import { useIntl } from "react-intl";
import { getText } from "../../../helpers/getText";

function a11yProps(index: number): { id: string; "aria-controls": string } {
    return {
        id: `scrollable-auto-tab-${index}`,
        "aria-controls": `scrollable-auto-tabpanel-${index}`,
    };
}

export type Tab = {
    caption: string;
    tabPanel: JSX.Element;
};

type Props = {
    tabs: Tab[];
    handleChange: (event: React.ChangeEvent<{}>, value: any) => void;
    value: string;
};

export default ({ tabs, handleChange, value }: Props): JSX.Element => {
    const { formatMessage } = useIntl();
    return (
        <Tabs
            value={value}
            onChange={handleChange}
            indicatorColor="primary"
            textColor="primary"
            variant="scrollable"
            scrollButtons="auto"
            aria-label="scrollable auto tabs example"
        >
            {tabs.map(({ caption }, index: number) => (
                <Tab
                    label={formatMessage({ id: getText(caption) })}
                    value={caption}
                    key={caption}
                    {...a11yProps(index)}
                />
            ))}
        </Tabs>
    );
};
