import styled from "styled-components/macro";
import { AppBar } from "@material-ui/core";

const StyledAppBar = styled(AppBar)`
    background-color: ${({ theme: { palette } }) =>
        palette.type === "light" ? "#f5f5f5" : palette.secondary.light};

    .MuiButtonBase-root {
        color: ${({ theme: { palette } }) =>
            palette.type === "light"
                ? "rgba(0, 0, 0, 0.54)"
                : "rgba(255, 255, 255, 0.7)"};
        &.Mui-selected {
            color: ${({ theme: { palette } }) =>
                palette.type === "light" ? "#1976d2" : "#90caf9"};
        }
    }
    .MuiTabs-indicator {
        background-color: ${({ theme: { palette } }) =>
            palette.type === "light" ? "#1976d2" : "#90caf9"};
    }
`;

export default StyledAppBar;
