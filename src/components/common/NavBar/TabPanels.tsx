import React from "react";
import { Typography, Box } from "@material-ui/core";
import { Tab } from "./Tabs";

const getClassName = (caption: string): string => {
    return `${caption
        .replace(/user_|search_/gi, "")
        .split(" ")
        .join("-")
        .toLowerCase()}-panel panel`;
};

type TabPanelProps = {
    children: React.ReactNode;
    value: string;
    index: string;
    className: string;
};

function TabPanel(props: TabPanelProps): JSX.Element {
    const { children, value, index, ...other } = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-auto-tabpanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
        >
            {value === index && <Box p={3}>{children}</Box>}
        </Typography>
    );
}

type Props = {
    tabs: Tab[];
    value: string;
};

export default ({ tabs, value }: Props): JSX.Element => (
    <>
        {tabs.map(({ caption, tabPanel }: Tab) => (
            <TabPanel
                className={getClassName(caption)}
                value={value}
                key={caption}
                index={caption}
            >
                {tabPanel}
            </TabPanel>
        ))}
    </>
);
