import React from "react";
import { Typography } from "@material-ui/core";
import { formatDate, formatNumber } from "../../../helpers/formatData";
import { useIntl } from "react-intl";

type Props = {
    info: any;
};

const CardCaption: React.FC<Props> = ({
    info: { type, creation_date, release_date, nb_fan, artist },
}) => {
    const { formatMessage } = useIntl();
    switch (type) {
        case "album":
            return (
                <Typography component="span" className="released">
                    {release_date
                        ? `${formatMessage({
                              id: "app.releasedOn",
                          })} ${formatDate(release_date)}`
                        : `by ${artist.name}`}
                </Typography>
            );
        case "playlist":
            return (
                <Typography component="span" className="released">
                    {`${formatMessage({ id: "app.createdOn" })} ${formatDate(
                        creation_date.split(" ")[0]
                    )}`}
                </Typography>
            );
        case "artist":
            return nb_fan ? (
                <Typography component="span" className="fans">
                    {formatNumber(nb_fan)} {formatMessage({ id: "app.fans" })}
                </Typography>
            ) : null;

        default:
            return null;
    }
};

export default CardCaption;
