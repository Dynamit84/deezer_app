import styled from "styled-components/macro";
import { Link } from "react-router-dom";
import { Card } from "@material-ui/core";

const StyledLink = styled(Link)`
    color: ${({ theme: { palette } }) =>
        palette.type === "light" ? "#32323d" : palette.common.white};
    text-decoration: none;
    font-size: 14px;
`;
const StyledCard = styled(Card)`
    background-color: ${(props) => props.theme.palette.background.paper};
`;

export default { StyledLink, Card: StyledCard };
