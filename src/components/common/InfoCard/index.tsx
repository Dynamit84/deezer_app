import React, { ReactElement } from "react";
import { CardContent, Typography, CardMedia } from "@material-ui/core";
import { Link } from "react-router-dom";
import PlayButton from "../Buttons/PlayButton";
import { connect } from "react-redux";
import { clickedPlay } from "../../../actions/actions";
import CardCaption from "./CardCaption";
import { ApplicationState } from "../../../reducers/types";
import { ItemInfo } from "../../../actions/types";
import { LinkUrl } from "../../../types";
import StyledComponents from "./styled";

type PassedProps = {
    linkUrl: LinkUrl;
    info: any;
};

type MapStateToProps = {
    trackId: number;
    entityId: number | null;
    isPlaying: boolean;
};

type MapDispatchToprops = {
    clickedPlay: ({ trackId, type, entityId }: Partial<ItemInfo>) => void;
};

type Props = MapStateToProps & MapDispatchToprops & PassedProps;

function InfoCard({
    info,
    clickedPlay,
    entityId,
    linkUrl,
    isPlaying,
}: Props): ReactElement {
    const { name, picture_medium, id, title, cover_medium } = info;

    const handleClick = (id: number) => {
        //if(isPlayerLoaded) {
        if (entityId === id) {
            if (isPlaying) {
                clickedPlay({ isPlaying: false });
                window.DZ.player.pause();
            } else {
                clickedPlay({ isPlaying: true });
                window.DZ.player.play();
            }
        } else {
            clickedPlay({ entityId: id, isPlaying: true });
            window.DZ.player.playAlbum(id);
        }
        //}
    };

    const { StyledLink, Card } = StyledComponents;

    return (
        <Card className="card">
            <CardContent className="container">
                <Link to={linkUrl(id)}>
                    <div className="media">
                        <CardMedia
                            className="image"
                            image={picture_medium || cover_medium}
                            title={name || title}
                        />
                    </div>
                </Link>
                <div className="actions-wrapper">
                    <PlayButton
                        onClick={handleClick.bind(null, id)}
                        isPlaying={isPlaying}
                        isVisible={entityId === id}
                    />
                </div>
                <Typography className="title">
                    <StyledLink className="link" to={linkUrl(id)}>
                        {name || title}
                    </StyledLink>
                </Typography>
                <CardCaption info={info} />
            </CardContent>
        </Card>
    );
}

const mapStateToProps = ({
    currentlyPlaying: { trackId, entityId, isPlaying },
}: ApplicationState): MapStateToProps => ({
    trackId,
    entityId,
    isPlaying,
});

export default connect(mapStateToProps, { clickedPlay })(InfoCard);
