import React from "react";
import "./styles.scss";
import { useIntl } from "react-intl";

export default function EmptyData(): JSX.Element {
    const { formatMessage } = useIntl();
    return (
        <div className="nodata-container">
            <div className="empty-img" />
            <span>{formatMessage({ id: "app.noResults" })}</span>
        </div>
    );
}
