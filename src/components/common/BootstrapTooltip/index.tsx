import { Tooltip } from "@material-ui/core";
import styled from "styled-components/macro";

const StyledTooltip = styled(Tooltip)`
    .MuiTooltip-arrow {
    }
`;

export default StyledTooltip;
