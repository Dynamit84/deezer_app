import React, { Component } from "react";
import "./styles.scss";
import { injectIntl, IntlShape } from "react-intl";

type State = {
    hasError: boolean;
};
type Props = {
    intl: IntlShape;
};

class ErrorBoundary extends Component<Props, State> {
    state: State = { hasError: false };

    static getDerivedStateFromError() {
        return { hasError: true };
    }

    render() {
        if (this.state.hasError) {
            return (
                <div className="error-container">
                    <div className="error-img" />
                    <span>
                        {this.props.intl.formatMessage({
                            id: "app.message.error",
                        })}
                    </span>
                </div>
            );
        }

        return this.props.children;
    }
}

export default injectIntl(ErrorBoundary);
