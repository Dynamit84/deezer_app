import styled, { keyframes } from "styled-components/macro";
import Loader from "../Loader";

const cssloadRotateOne = keyframes`
0% {
    transform: rotateX(35deg) rotateY(-45deg) rotateZ(0deg);
}
100% {
    transform: rotateX(35deg) rotateY(-45deg) rotateZ(360deg);
}`;
const cssloadRotateTwo = keyframes`
0% {
    transform: rotateX(50deg) rotateY(10deg) rotateZ(0deg);
}
100% {
    transform: rotateX(50deg) rotateY(10deg) rotateZ(360deg);
}`;
const cssloadRotateThree = keyframes`
0% {
    transform: rotateX(35deg) rotateY(55deg) rotateZ(0deg);
}
100% {
    transform: rotateX(35deg) rotateY(55deg) rotateZ(360deg);
}`;

const StyledLoader = styled(Loader)`
    position: relative;
    left: calc(50% - 31px);
    width: 120px;
    height: 120px;
    border-radius: 50%;
    perspective: 780px;

    .inner-content {
        width: 36px;
        height: 36px;
        position: absolute;
        left: calc(50% - 18px);
        top: 40px;
    }

    .cssload-inner {
        position: absolute;
        width: 100%;
        height: 100%;
        box-sizing: border-box;
        border-radius: 50%;

        &.cssload-one {
            left: 0;
            top: 0;
            animation: ${cssloadRotateOne} 2s linear infinite;
            border-bottom: 3px solid
                ${({ theme: { palette } }) =>
                    palette.type === "light"
                        ? palette.common.black
                        : palette.common.white};
        }

        &.cssload-two {
            right: 0;
            top: 0;
            animation: ${cssloadRotateTwo} 2s linear infinite;
            border-right: 3px solid
                ${({ theme: { palette } }) =>
                    palette.type === "light"
                        ? palette.common.black
                        : palette.common.white};
        }

        &.cssload-three {
            right: 0;
            bottom: 0;
            animation: ${cssloadRotateThree} 2s linear infinite;
            border-top: 3px solid
                ${({ theme: { palette } }) =>
                    palette.type === "light"
                        ? palette.common.black
                        : palette.common.white};
        }
    }
`;

export default StyledLoader;
