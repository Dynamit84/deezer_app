import React from "react";
import { ReactComponent as Spinner } from "../../../images/note.svg";

export default ({ className }: { className?: string }): JSX.Element => (
    <div className={`cssload-loader ${className}`}>
        <Spinner className="inner-content" />
        <div className="cssload-inner cssload-one"></div>
        <div className="cssload-inner cssload-two"></div>
        <div className="cssload-inner cssload-three"></div>
    </div>
);
