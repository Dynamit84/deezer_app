import React from "react";

type Props = {
    info: string[];
};

const InfoList: React.FC<Props> = ({ info }) => {
    return (
        <ul className="info-list">
            {info.map((item: string, index: number) => (
                <li className="list-item" key={index}>
                    {item}
                </li>
            ))}
        </ul>
    );
};

export default InfoList;
