import styled from "styled-components/macro";
import { InputLabel, Select } from "@material-ui/core";
import { Palette } from "@material-ui/core/styles/createPalette";

const getColor = (palette: Palette) =>
    palette.type === "light" ? palette.common.black : palette.common.white;

const StyledInputLabel = styled(InputLabel)`
    color: ${({ theme: { palette } }) => getColor(palette)};
    opacity: 0.54;
`;

const StyledSelect = styled(Select)`
    color: ${({ theme: { palette } }) => getColor(palette)};
    width: 100%;
    &.MuiOutlinedInput-root {
        fieldset {
            border-color: ${({ theme: { palette } }) =>
                palette.type === "light"
                    ? "rgba(0, 0, 0, 0.23)"
                    : "rgba(255, 255, 255, 0.23)"};
        }
        &:hover fieldset {
            border-color: ${({ theme: { palette } }) => getColor(palette)};
        }
        svg {
            fill: ${({ theme: { palette } }) => getColor(palette)};
        }
    }
`;

export default { InputLabel: StyledInputLabel, Select: StyledSelect };
