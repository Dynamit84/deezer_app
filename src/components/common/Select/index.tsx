import React, { FC } from "react";
import { MenuItem } from "@material-ui/core";
import StyledComponents from "./styled";
import { createMuiTheme } from "@material-ui/core/styles";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { ApplicationState } from "../../../reducers/types";
import { connect } from "react-redux";
import { PaletteType } from "@material-ui/core";

type MapStateToProps = {
    themeType: PaletteType;
};

type Props = {
    selected: string;
    onChange: (value: unknown) => void;
    items: any[];
    labelWidth?: number;
    handleTooltip?: (bool: boolean) => void;
} & MapStateToProps;

const SimpleSelect: FC<Props> = ({
    items,
    selected,
    onChange,
    labelWidth,
    handleTooltip,
    themeType,
}) => {
    const handleChange = (
        event: React.ChangeEvent<{ name?: string; value: unknown }>
    ) => {
        onChange(event.target.value);
    };
    const theme = createMuiTheme({
        overrides: {
            MuiPaper: {
                root: {
                    color: themeType === "light" ? "#000" : "#fff",
                    backgroundColor: themeType === "light" ? "#fff" : "#424242",
                },
            },
            MuiMenuItem: {
                root: {
                    "&$selected, &$selected:hover": {
                        backgroundColor:
                            themeType === "light"
                                ? "rgba(0, 0, 0, 0.08)"
                                : "rgba(255, 255, 255, 0.16)",
                    },
                    "&:hover:not($selected)": {
                        backgroundColor:
                            themeType === "light"
                                ? "rgba(0, 0, 0, 0.04)"
                                : "rgba(255, 255, 255, 0.08)",
                    },
                },
            },
        },
    });
    const { Select } = StyledComponents;

    return (
        <MuiThemeProvider theme={theme}>
            <Select
                labelId="outlined-label"
                id="select-outlined"
                value={selected}
                onChange={handleChange}
                labelWidth={labelWidth}
                onMouseEnter={() => {
                    handleTooltip && handleTooltip(true);
                }}
                onMouseLeave={() => {
                    handleTooltip && handleTooltip(false);
                }}
                onOpen={() => {
                    handleTooltip && handleTooltip(false);
                }}
            >
                {items.map(
                    ({
                        id,
                        name,
                        Icon,
                    }: {
                        id: number;
                        name: string;
                        Icon?: React.ReactType;
                    }) => (
                        <MenuItem key={id} value={name}>
                            {Icon && <Icon />}
                            <span>{name}</span>
                        </MenuItem>
                    )
                )}
            </Select>
        </MuiThemeProvider>
    );
};

const mapStateToProps = ({
    user: { theme },
}: ApplicationState): MapStateToProps => ({
    themeType: theme,
});

export default connect(mapStateToProps)(SimpleSelect);
