import React from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { formatNumber } from "../../../helpers/formatData";

type Props = {
    caption: string;
    withNumber?: boolean;
    total?: number | null;
};

const TabCaption: React.FC<Props> = ({ caption, withNumber, total }) => {
    const useStyles = makeStyles({
        "tab-caption": {
            marginRight: 15,
            whiteSpace: "nowrap",
        },
        border: {
            height: 1,
            backgroundColor: "#dedee3",
            width: "100%",
        },
        "caption-wrapper": {
            display: "flex",
            marginBottom: 20,
            alignItems: "center",
        },
    });
    const classes = useStyles();

    return withNumber ? (
        <Typography variant="h6">
            {caption} {total && `(${formatNumber(total as number)})`}
        </Typography>
    ) : (
        <div className={classes["caption-wrapper"]}>
            <Typography className={classes["tab-caption"]} variant="h5">
                {caption}
            </Typography>
            <div className={classes.border}></div>
        </div>
    );
};

export default TabCaption;
