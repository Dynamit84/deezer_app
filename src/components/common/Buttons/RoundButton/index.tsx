import React from "react";
import IconButton from "@material-ui/core/IconButton";
import styled from "styled-components/macro";

type Props = {
    onClick: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
    btnClass?: string;
    children: React.ReactNode;
    className?: string;
};

const StyledIconButton = styled(IconButton)`
    width: 36px;
    height: 36px;
    z-index: 100;
    box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.25);
    transition: transform 225ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
    padding: 0;
    background-color: #fff;
    position: absolute;
    color: #000;
    &:hover {
        background-color: #fff;
        .pause-icon {
            display: inline;
        }
    }

    &:hover {
        .eqzr-icon {
            display: none;
        }
    }
    .pause-icon {
        width: 18px;
        height: 18px;
        display: none;
    }
    .arrow-icon {
        width: 18px;
        height: 18px;
    }
    &.play,
    &.zoom {
        &:hover {
            transform: scale3d(1.2, 1.2, 1.2);
        }
    }
    &.close {
        &:hover {
            background-color: #efefef;
        }
    }
`;

export default ({
    className,
    onClick,
    children,
    btnClass,
}: Props): JSX.Element => {
    return (
        <StyledIconButton
            className={`${className || ""} ${btnClass || ""}`.trim()}
            onClick={onClick}
        >
            {children}
        </StyledIconButton>
    );
};
