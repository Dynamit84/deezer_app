import React, { FC, ReactElement } from "react";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import PauseIcon from "@material-ui/icons/Pause";
import RoundButton from "../RoundButton";
import eqzrBlack from "../../../../images/eqzr-black.gif";
import eqzrWhite from "../../../../images/eqzr-white.gif";
import { ApplicationState } from "../../../../reducers/types";
import { PaletteType } from "@material-ui/core";
import { connect } from "react-redux";

type MapStateToProps = {
    theme: PaletteType;
    isPlayerLoaded: boolean;
};

type Props = {
    onClick: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
    isPlaying: boolean;
    isVisible?: boolean;
} & MapStateToProps;

const PlayButton: FC<Props> = ({
    onClick,
    isPlaying,
    theme,
    isVisible,
    isPlayerLoaded,
}) => {
    const getButtonContent = (): ReactElement => {
        switch (true) {
            case isPlaying && isVisible:
                return (
                    <>
                        <img
                            src={theme === "light" ? eqzrWhite : eqzrBlack}
                            className="eqzr-icon"
                            alt=""
                        />
                        <PauseIcon className="pause-icon" />
                    </>
                );

            case !isPlaying && isVisible:
                return (
                    <PauseIcon
                        className="pause-icon"
                        style={{ display: "inline-block" }}
                    />
                );

            default:
                return <PlayArrowIcon className="arrow-icon" />;
        }
    };

    return isPlayerLoaded ? (
        <RoundButton onClick={onClick} btnClass="play">
            {getButtonContent()}
        </RoundButton>
    ) : null;
};

const mapStateToProps = ({
    user: { theme },
    player: { isLoaded },
}: ApplicationState): MapStateToProps => ({
    theme,
    isPlayerLoaded: isLoaded,
});

export default connect(mapStateToProps)(PlayButton);
