import React, { FC } from "react";
import RoundButton from "../RoundButton";
import { Brightness4, Brightness5 } from "@material-ui/icons";
import { ApplicationState } from "../../../../reducers/types";
import { toggleTheme } from "../../../../actions/actions";
import { connect } from "react-redux";
import { PaletteType } from "@material-ui/core";
import styled from "styled-components/macro";

const StyledRoundButton = styled(RoundButton)`
    color: ${(props) => props.theme.palette.common.white};
    margin-left: 15px;
    position: relative;
    background-color: transparent;
    box-shadow: none;
    &:hover {
        background-color: ${({ theme: { palette } }) =>
            palette.type === "light"
                ? "rgba(0, 0, 0, 0.09)"
                : "rgba(255, 255, 255, 0.08)"};
    }
    width: 44px;
    height: 44px;
`;

type MapStateToProps = {
    theme: PaletteType;
};

type MapDispatchToProps = {
    toggleTheme: () => void;
};

type Props = MapStateToProps & MapDispatchToProps;

const ThemeButton: FC<Props> = ({ theme, toggleTheme }) => {
    const handleClick = () => {
        toggleTheme();
    };
    const Icon = theme === "dark" ? Brightness5 : Brightness4;
    return (
        <StyledRoundButton onClick={handleClick}>
            <Icon />
        </StyledRoundButton>
    );
};

const mapStateToProps = ({
    user: { theme },
}: ApplicationState): MapStateToProps => ({
    theme,
});

export default connect(mapStateToProps, { toggleTheme })(ThemeButton);
