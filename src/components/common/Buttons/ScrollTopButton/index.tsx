import React, { useEffect, useState, ReactElement } from "react";
import RoundButton from "../RoundButton";
import ArrowUpwardIcon from "@material-ui/icons/ArrowUpward";
import { animateScroll as scroll } from "react-scroll";
import styled from "styled-components/macro";

const StyledRoundButton = styled(RoundButton)`
    position: fixed;
    right: 10px;
    bottom: 135px;
    background-color: #2d96c8;
    width: 40px;
    height: 40px;
    &:hover {
        transform: scale3d(1.2, 1.2, 1.2);
        background-color: #2d96c8;
    }
`;

function ScrollTopButton(): ReactElement | null {
    const [isVisible, setIsVisible] = useState<boolean>(false);
    const handleClick = () => {
        scroll.scrollToTop({
            duration: 150,
            smooth: "easeInOutQuint",
        });
    };

    const toogleVisibility = () => {
        if (
            document.body.scrollTop > 700 ||
            document.documentElement.scrollTop > 700
        ) {
            setIsVisible(true);
        } else {
            setIsVisible(false);
        }
    };

    useEffect(() => {
        window.addEventListener("scroll", toogleVisibility);

        return () => {
            window.removeEventListener("scroll", toogleVisibility);
        };
    }, []);

    return isVisible ? (
        <StyledRoundButton onClick={handleClick}>
            <ArrowUpwardIcon className="scroll-top-icon" />
        </StyledRoundButton>
    ) : null;
}

export default ScrollTopButton;
