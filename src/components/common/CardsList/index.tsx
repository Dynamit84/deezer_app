import React, { useRef, useCallback, useEffect, useState, FC } from "react";
import InfoCard from "../InfoCard";
import Grid from "@material-ui/core/Grid";
import { Loader } from "../Loader";
import { DataForFetching, GeneralData, LinkUrl } from "../../../types";

type Props = {
    linkUrl: LinkUrl;
    dataForFetching: DataForFetching;
    data: Partial<GeneralData>;
};

const CardsList: FC<Props> = ({
    linkUrl,
    data,
    dataForFetching: { entity, id, fetchData },
}) => {
    const observer = useRef<IntersectionObserver | null>(null);
    const { data: items = [], isLoading, next, errorMsg } = data;
    const hasMore = Boolean(next);
    const lastTrackElementRef = useCallback(
        (node) => {
            if (isLoading) return;
            if (observer.current) {
                observer.current.disconnect();
            }

            observer.current = new IntersectionObserver(
                (entries) => {
                    if (entries[0].isIntersecting && hasMore) {
                        fetchData(entity, { id, params: next });
                    }
                },
                { threshold: 1 }
            );
            if (node) {
                observer.current.observe(node);
            }
        },
        [isLoading, hasMore, next, fetchData, entity, id]
    );

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [_, setError] = useState();

    useEffect(() => {
        errorMsg &&
            setError(() => {
                throw errorMsg;
            });
    }, [errorMsg, setError]);

    return (
        <Grid className="grid" container spacing={1} justify="space-around">
            {items.map((item) => (
                <Grid
                    key={item.id as number | string}
                    item
                    className="grid-item"
                >
                    <InfoCard info={item} linkUrl={linkUrl} />
                </Grid>
            ))}
            {isLoading && (
                <div className="loader-container">
                    <Loader />
                </div>
            )}
            {!isLoading && hasMore && <Grid ref={lastTrackElementRef} />}
        </Grid>
    );
};

export default CardsList;
