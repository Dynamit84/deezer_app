import React from "react";
import { Typography, Avatar } from "@material-ui/core";
import { Link } from "react-router-dom";
import InfoList from "../InfoList";
import Buttons from "../Buttons";
import ZoomInIcon from "@material-ui/icons/ZoomIn";
import BootstrapTooltip from "../BootstrapTooltip";
import styled from "styled-components/macro";
import { ItemInfo } from "../../../actions/types";
import { Track } from "../../../types";

const StyledLink = styled(Link)`
    color: ${(props) => props.theme.palette.text.primary};
    margin-left: 10px;
`;

type Props = {
    infoListData: string[];
    entity: {
        id: number;
        title: string;
        cover_medium?: string;
        description?: string;
        picture_medium?: string;
        creator?: {
            name: string;
        };
        artist?: {
            name: string;
            picture_medium: string;
        };
        tracks: {
            data: Track[];
        };
    };
    linkUrl: string;
    onModalInteract?: () => void;
    playingEntityId: number | null;
    isPlaying: boolean;
    clickedPlay: ({ trackId, type, entityId }: Partial<ItemInfo>) => void;
};

export default ({
    infoListData,
    entity,
    linkUrl,
    onModalInteract,
    playingEntityId,
    isPlaying,
    clickedPlay,
}: Props) => {
    const { RoundButton, PlayButton } = Buttons;
    const {
        title,
        cover_medium,
        description,
        creator,
        artist,
        picture_medium,
        id,
        tracks: { data },
    } = entity;

    const name = (creator && creator.name) || (artist && artist.name);
    const picture = artist && artist.picture_medium;
    //const [isPlaying, setIsPlaying] = useState(false);

    const handleClick = (id: number) => {
        //if(isPlayerLoaded) {
        if (playingEntityId === id) {
            if (isPlaying) {
                clickedPlay({ isPlaying: false });
                window.DZ.player.pause();
            } else {
                clickedPlay({ isPlaying: true });
                window.DZ.player.play();
            }
        } else {
            clickedPlay({ trackId: data[0].id, entityId: id, isPlaying: true });
            console.log(window.DZ.player);
            window.DZ.player.playTracks(["528330411"]);
        }
        //}
    };
    return (
        <div className="entity-preview">
            <div className="cover-container">
                <figure className="entity-cover">
                    <img src={picture_medium || cover_medium} alt={title} />
                </figure>
                <PlayButton
                    onClick={handleClick.bind(null, id)}
                    isPlaying={isPlaying}
                    isVisible={playingEntityId === id}
                />
                {onModalInteract && (
                    <RoundButton btnClass="zoom" onClick={onModalInteract}>
                        <ZoomInIcon />
                    </RoundButton>
                )}
            </div>
            <div className="info-container">
                <div>
                    <Typography variant="h4" className="entity-title">
                        {title}
                    </Typography>
                    <div className="link-container">
                        <BootstrapTooltip title={name} placement="top" arrow>
                            <Link className="avatar-link" to={linkUrl}>
                                <Avatar alt={name} src={picture} />
                            </Link>
                        </BootstrapTooltip>
                        <StyledLink to={linkUrl}>{name}</StyledLink>
                    </div>
                    {description && (
                        <Typography className="entity-description">
                            {description}
                        </Typography>
                    )}
                </div>
                <InfoList info={infoListData} />
            </div>
        </div>
    );
};
