import React from "react";
import { ListItem, ListItemAvatar, Avatar } from "@material-ui/core";
import { TComment } from "../../../types";
import { ListItemText, Link, StyledCommentText } from "./styled";

type Props = {
    comment: TComment;
    linkUrl: string;
};

export default ({ comment: { text, author }, linkUrl }: Props): JSX.Element => {
    return (
        <ListItem alignItems="flex-start">
            <ListItemAvatar>
                <Avatar alt={author.name} src={author.picture_small} />
            </ListItemAvatar>
            <ListItemText
                primary={
                    <div>
                        <Link to={linkUrl}>{author.name}</Link>
                    </div>
                }
                secondary={
                    <>
                        <StyledCommentText>
                            {text}
                        </StyledCommentText>
                    </>
                }
            />
        </ListItem>
    );
};
