import styled from "styled-components/macro";
import { Link } from "react-router-dom";

const StyledLink = styled(Link)`
    color: ${(props) => props.theme.palette.text.primary};
    font-size: 12px;
    font-weight: 700;
`;

export default StyledLink;
