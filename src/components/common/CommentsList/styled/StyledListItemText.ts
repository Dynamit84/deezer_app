import styled from "styled-components/macro";
import { ListItemText } from "@material-ui/core";

const StyledListItemText = styled(ListItemText)`
    border-radius: 3px;
    padding: 10px;
    background-color: ${({ theme: { palette } }) =>
        palette.type === "light" ? "#ececec" : palette.secondary.light};
`;

export default StyledListItemText;
