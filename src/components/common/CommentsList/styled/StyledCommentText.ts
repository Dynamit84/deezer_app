import styled from "styled-components/macro";

const StyledCommentText = styled.span`
    color: ${({ theme: { palette } }) =>
        palette.type === "light" ? "rgb(114, 114, 125)" : "rgb(146, 146, 157)"};
    font-size: 13px;
`;

export default StyledCommentText;
