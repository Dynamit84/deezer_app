import React, { useState, useEffect } from "react";
import Comment from "./Comment";
import List from "@material-ui/core/List";
import withLoader from "../../../hocs/withLoader";
import { TComment } from "../../../types";
import { GeneralData, DataForFetching } from "../../../types";
import { getUserUrl } from "../../../helpers/getUrls";

type Props = {
    data: Partial<GeneralData>;
    isLoading: boolean;
    dataForFetching: DataForFetching;
};

function CommentsList({ data }: Props): JSX.Element {
    const { data: comments, errorMsg } = data;
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [_, setError] = useState();

    useEffect(() => {
        errorMsg &&
            setError(() => {
                throw errorMsg;
            });
    }, [errorMsg, setError]);
    return (
        <List>
            {(comments as TComment[]).map((comment: TComment) => (
                <Comment
                    linkUrl={getUserUrl(comment.author.id)}
                    key={comment.id}
                    comment={comment}
                />
            ))}
        </List>
    );
}

export default withLoader(CommentsList);
