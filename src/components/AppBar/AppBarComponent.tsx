import React, { ReactElement } from "react";
import { AppBar, Toolbar, Typography } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import { appBarLabel } from "../../constants/texts";
import { Link } from "react-router-dom";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import SearchContext from "./Search/SearchContext";
import { DataForSearchList } from "./index";
import Select from "../common/Select";
import ThemeButton from "../common/Buttons/ThemeButton";
import BootstrapTooltip from "../common/BootstrapTooltip";
import { InputBase, SearchResults, ClearBtn } from "./styled";

type Props = {
    isClearVisible: boolean;
    isResultVisible: boolean;
    isAllDataLoading: boolean;
    clearSearch: () => void;
    handleFocus: () => void;
    handleChange: (
        event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
    ) => void;
    inputValue: string;
    dataForSearchList: DataForSearchList;
    language: string;
    onChange: (lang: unknown) => void;
    items: any[];
    themeBtnTitle: string;
    changeLangTitle: string;
    className?: string;
    themeType: string;
    isTooltipOpen: boolean;
    handleTooltip: (bool: boolean) => void;
};

export default function AppBarComponent({
    isClearVisible,
    isResultVisible,
    clearSearch,
    handleFocus,
    inputValue,
    isAllDataLoading,
    handleChange,
    dataForSearchList,
    language,
    onChange,
    items,
    themeBtnTitle,
    changeLangTitle,
    className,
    isTooltipOpen,
    handleTooltip,
}: Props): ReactElement {
    return (
        <div className={className}>
            <AppBar position="static">
                <Toolbar>
                    <Link className="appbar-logo" to="/"></Link>
                    <Typography className="appbar-title" variant="h6" noWrap>
                        {appBarLabel}
                    </Typography>
                    <div className="appbar-search">
                        <div className="appbar-search-icon">
                            <SearchIcon />
                        </div>
                        <InputBase
                            placeholder="Search…"
                            id="searchInput"
                            className="appbar-input-root"
                            onChange={handleChange}
                            onFocus={handleFocus}
                            value={inputValue}
                            inputProps={{ "aria-label": "search" }}
                            autoComplete="off"
                        />
                        {isClearVisible && (
                            <ClearBtn size="small" onClick={clearSearch}>
                                <HighlightOffIcon fontSize="small" />
                            </ClearBtn>
                        )}
                    </div>
                    <BootstrapTooltip
                        placement="bottom"
                        title={changeLangTitle}
                        arrow
                        open={isTooltipOpen}
                    >
                        <div>
                            <Select
                                items={items}
                                selected={language}
                                onChange={onChange}
                                handleTooltip={handleTooltip}
                            />
                        </div>
                    </BootstrapTooltip>
                    <BootstrapTooltip
                        placement="bottom"
                        title={themeBtnTitle}
                        arrow
                    >
                        <div>
                            <ThemeButton />
                        </div>
                    </BootstrapTooltip>
                    {isResultVisible && (
                        <SearchContext.Provider value={dataForSearchList}>
                            <SearchResults isLoading={isAllDataLoading} />
                        </SearchContext.Provider>
                    )}
                </Toolbar>
            </AppBar>
        </div>
    );
}
