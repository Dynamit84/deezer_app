import React, { useState, useCallback, useEffect, FC } from "react";
import {
    SEARCH_ARTISTS,
    SEARCH_PLAYLISTS,
    SEARCH_TRACKS,
    SEARCH_ALBUMS,
} from "../../constants/texts";
import { debounce } from "debounce";
import { connect } from "react-redux";
import {
    fetchSearchData,
    changeLanguage,
    updateLocalStorage,
} from "../../actions/actions";
import { withRouter, RouteComponentProps } from "react-router";
import { ApplicationState, SearchState, UserState } from "../../reducers/types";
import getLanguageName from "../../helpers/getLanguageName";
import LOCALES from "../../i18n/locales";
import Flags from "country-flag-icons/react/3x2";
import { useIntl } from "react-intl";
import { AppBar } from "./styled";

type MapStateToProps = {
    search: SearchState;
    user: UserState;
};

type MapDispatchToProps = {
    fetchSearchData: (entities: string[], value: string) => void;
    changeLanguage: (lang: string) => void;
    updateLocalStorage: (user: UserState) => void;
};

type Props = MapStateToProps &
    MapDispatchToProps &
    RouteComponentProps<{}, {}, { searchQuery: string }>;

export type DataForSearchList = {
    searchResults: SearchState;
    searchQuery: string;
    showSpinner: boolean;
};

const SearchAppBar: FC<Props> = ({
    fetchSearchData,
    search: { isAllDataLoading },
    search,
    location: { state },
    changeLanguage,
    updateLocalStorage,
    user,
    user: { language, theme },
}) => {
    const [inputValue, setInputValue] = useState("");
    const [isClearVisible, setClearVisible] = useState(false);
    const [isResultVisible, setResultVisible] = useState(false);
    const [isTooltipOpen, setIsTooltipOpen] = useState(false);
    const handleTooltip = (bool: boolean) => {
        setIsTooltipOpen(bool);
    };
    const dataForSearchList = {
        searchResults: search,
        searchQuery: inputValue,
        showSpinner: isAllDataLoading,
    };

    const currentLang = getLanguageName(language);

    const { formatMessage } = useIntl();
    const themeButtonTitle = formatMessage({ id: "app.toggletheme" });
    const changeLangTitle = formatMessage({ id: "app.changelanguage" });

    const getSearchedData = (value: string) => {
        fetchSearchData(
            [SEARCH_ARTISTS, SEARCH_PLAYLISTS, SEARCH_TRACKS, SEARCH_ALBUMS],
            value
        );
    };

    const debouncedFn = useCallback(
        debounce((value: string) => {
            if (value === "") {
                setResultVisible(false);
                return;
            }
            getSearchedData(value);
            setResultVisible(true);
        }, 1000),
        []
    );

    const handleChange = (
        event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
    ) => {
        setInputValue(event.target.value);
        //const trimValue = event.target.value.trim();
        //debouncedFn(trimValue);
    };

    const clearSearch = () => {
        setInputValue("");
        setResultVisible(false);
    };

    useEffect(() => {
        inputValue ? setClearVisible(true) : setClearVisible(false);
        debouncedFn(inputValue.trim());
    }, [inputValue, debouncedFn]);

    useEffect(() => {
        state && setInputValue(state.searchQuery);
    }, [state]);

    useEffect(() => {
        updateLocalStorage(user);
    }, [user, updateLocalStorage]);

    const escFunction = useCallback(
        (event) => {
            if (event.keyCode === 27) {
                isResultVisible && setResultVisible(false);
            }
        },
        [isResultVisible]
    );

    const clickOutsideSearchResults = useCallback(
        (event) => {
            if (event.target.id === "searchInput") return;
            isResultVisible && setResultVisible(false);
        },
        [isResultVisible]
    );

    useEffect(() => {
        document.addEventListener("keydown", escFunction, false);

        return () => {
            document.removeEventListener("keydown", escFunction, false);
        };
    }, [escFunction]);

    useEffect(() => {
        document.addEventListener("click", clickOutsideSearchResults, false);

        return () => {
            document.removeEventListener(
                "click",
                clickOutsideSearchResults,
                false
            );
        };
    }, [clickOutsideSearchResults]);

    const handleFocus = () => {
        inputValue && setResultVisible(true);
    };

    const onLanguageChange = (languageName: unknown) => {
        const language = LOCALES.filter(
            (locale) => locale.name === languageName
        )[0];
        changeLanguage(language.code);
    };

    const formLanguagesHash = () => {
        return LOCALES.map((locale) => {
            return {
                ...locale,
                Icon:
                    locale.code === "en-US"
                        ? Flags.US
                        : Flags[locale.code.toUpperCase()],
            };
        });
    };

    return (
        <AppBar
            isClearVisible={isClearVisible}
            isResultVisible={isResultVisible}
            isAllDataLoading={isAllDataLoading}
            clearSearch={clearSearch}
            handleFocus={handleFocus}
            handleChange={handleChange}
            inputValue={inputValue}
            dataForSearchList={dataForSearchList}
            language={currentLang}
            onChange={onLanguageChange}
            items={formLanguagesHash()}
            themeBtnTitle={themeButtonTitle}
            changeLangTitle={changeLangTitle}
            themeType={theme}
            isTooltipOpen={isTooltipOpen}
            handleTooltip={handleTooltip}
        />
    );
};

const mapStateToProps = ({
    search,
    user,
}: ApplicationState): MapStateToProps => ({
    search,
    user,
});

export default withRouter(
    connect<MapStateToProps, MapDispatchToProps, {}, ApplicationState>(
        mapStateToProps,
        {
            fetchSearchData,
            changeLanguage,
            updateLocalStorage,
        }
    )(SearchAppBar)
);
