import React from "react";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import { Link } from "react-router-dom";
import SearchContext from "./SearchContext";
import { useIntl } from "react-intl";
import { getText } from "../../../helpers/getText";

type Props = {
    entity: string;
    total?: number | null;
};

const SearchItem: React.FC<Props> = ({ entity, total }) => {
    const { formatMessage } = useIntl();
    return Boolean(total) ? (
        <SearchContext.Consumer>
            {({ searchQuery }) => {
                return (
                    <li className="search-item">
                        <Link
                            to={{
                                pathname: "/search",
                                state: { entity, searchQuery },
                            }}
                            className="search-item-link"
                        >
                            <span className="search-item-link-content">
                                {formatMessage({ id: getText(entity) })} (
                                {total}){" "}
                                <ChevronRightIcon className="search-item-arrow-icon" />
                            </span>
                        </Link>
                    </li>
                );
            }}
        </SearchContext.Consumer>
    ) : null;
};

export default SearchItem;
