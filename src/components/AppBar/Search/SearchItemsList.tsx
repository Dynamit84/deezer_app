import React from "react";
import {
    SEARCH_TRACKS,
    SEARCH_ALBUMS,
    SEARCH_PLAYLISTS,
    SEARCH_ARTISTS,
} from "../../../constants/texts";
import SearchItem from "./SearchItem";
import SearchContext from "./SearchContext";
import withLoader from "../../../hocs/withLoader";
import { SearchState } from "../../../reducers/types";
import { useIntl } from "react-intl";

const SearchItemsList: React.FC = () => {
    const isAnythingFound = ({
        tracks,
        artists,
        albums,
        playlists,
    }: SearchState): boolean => {
        return Boolean(
            (tracks && tracks.total && tracks.total > 0) ||
                (artists && artists.total && artists.total > 0) ||
                (albums && albums.total && albums.total > 0) ||
                (playlists && playlists.total && playlists.total > 0)
        );
    };
    const { formatMessage } = useIntl();
    return (
        <SearchContext.Consumer>
            {({
                searchResults,
                searchResults: { tracks, artists, albums, playlists },
                searchQuery,
            }) => {
                return isAnythingFound(searchResults) ? (
                    <ul>
                        <SearchItem
                            entity={SEARCH_TRACKS}
                            total={tracks.total}
                        />
                        <SearchItem
                            entity={SEARCH_ALBUMS}
                            total={albums.total}
                        />
                        <SearchItem
                            entity={SEARCH_PLAYLISTS}
                            total={playlists.total}
                        />
                        <SearchItem
                            entity={SEARCH_ARTISTS}
                            total={artists.total}
                        />
                    </ul>
                ) : (
                    <span className="search-empty">
                        {formatMessage({ id: "app.message.notfound" })}: "
                        {searchQuery}"
                    </span>
                );
            }}
        </SearchContext.Consumer>
    );
};

export default withLoader(SearchItemsList);
