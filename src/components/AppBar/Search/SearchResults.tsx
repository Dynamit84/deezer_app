import React from "react";
import SearchItemsList from "./SearchItemsList";

type Props = {
    isLoading: boolean;
    className?: string;
};

const SearchResults: React.FC<Props> = ({ isLoading, className }: Props) => {
    return (
        <div className={className}>
            <SearchItemsList isLoading={isLoading} />
        </div>
    );
};

export default SearchResults;
