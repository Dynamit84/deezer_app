import React from "react";
import { DataForSearchList } from "../index";
const SearchContext = React.createContext<DataForSearchList>(
    {} as DataForSearchList
);
export default SearchContext;
