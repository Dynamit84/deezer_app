import styled from "styled-components/macro";
import { InputBase } from "@material-ui/core";

const StyledInputBase = styled(InputBase)`
    .MuiInputBase-input {
        padding: 6px 35px 7px 55px;
        width: 260px;
    }
`;
export default StyledInputBase;
