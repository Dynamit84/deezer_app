import styled from "styled-components/macro";
import { IconButton } from "@material-ui/core";

const StyledClearBtn = styled(IconButton)`
    color: #fff;
    position: absolute;
    right: 5px;
`;
export default StyledClearBtn;
