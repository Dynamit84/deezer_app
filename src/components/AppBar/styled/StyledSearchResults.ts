import styled, { keyframes } from "styled-components/macro";
import SearchResults from "../Search/SearchResults";

const swing = keyframes` {
    0% {
        transform: translateX(0);
    }
    50% {
        transform: translateX(6px);
    }
    100% {
        transform: translateX(0);
    }
    }`;

const StyledSearchResults = styled(SearchResults)`
    position: absolute;
    width: 440px;
    right: 108px;
    top: 50px;
    background-color: ${({ theme: { palette } }) =>
        palette.type === "light"
            ? palette.common.white
            : palette.primary.light};
    box-sizing: border-box;
    padding: 15px;
    border-radius: 5px;
    box-shadow: 0 4px 20px 0 rgba(25, 25, 34, 0.44);
    .cssload-loader {
        left: calc(50% - 40px);
        width: 80px;
        height: 80px;

        svg {
            width: 20px;
            height: 20px;
            left: calc(50% - 10px);
            top: calc(50% - 10px);
        }
    }

    .search-item {
        padding: 0 10px;
        display: flex;
        align-items: center;
        &-link {
            height: 40px;
            border-radius: 4px;
            display: block;
            width: 100%;
            color: ${({ theme: { palette } }) =>
                palette.type === "light"
                    ? palette.common.black
                    : palette.common.white};
            &:hover {
                background-color: ${({ theme: { palette } }) =>
                    palette.secondary.main};
                .search-item-arrow-icon {
                    animation: ${swing} 0.3s;
                }
            }
        }
        &-link-content {
            display: flex;
            align-items: center;
            height: inherit;
            padding: 0 10px;
        }
    }

    .search-empty {
        word-wrap: break-word;
        color: ${({ theme: { palette } }) =>
            palette.type === "light"
                ? palette.common.black
                : palette.common.white};
    }
`;

export default StyledSearchResults;
