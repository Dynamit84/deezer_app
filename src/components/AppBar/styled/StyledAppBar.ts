import styled from "styled-components/macro";
import AppBarComponent from "../AppBarComponent";
import { fade } from "@material-ui/core/styles";
import DarkLogo from "../../../images/logo_dark.png";
import LightLogo from "../../../images/logo_light.png";

const StyledAppBar = styled(AppBarComponent)`
    flex-grow: 1;
    position: sticky;
    top: 0;
    z-index: 120;
    header {
        background-color: ${(props) => props.theme.palette.primary.main};
    }
    .appbar-title {
        flex-grow: 1;
        margin-left: 20px;
    }
    .appbar-search {
        display: flex;
        align-items: center;
        position: relative;
        border-radius: ${(props) => props.theme.shape.borderRadius}px;
        background-color: ${(props) =>
            fade(props.theme.palette.common.white, 0.15)};
        &:hover {
            background-color: ${(props) =>
                fade(props.theme.palette.common.white, 0.25)};
        }
        margin-left: 0;
        margin-right: 15px;
        width: auto;
    }
    .appbar-search-icon {
        width: ${(props) => props.theme.spacing(7)}px;
        height: 100%;
        position: absolute;
        pointer-events: none;
        display: flex;
        align-items: center;
        justify-content: center;
        z-index: 1000;
    }
    .appbar-input-root {
        color: inherit;
        transition: ${(props) => props.theme.transitions.create("width")};
        color: #fff;
    }
    .appbar-logo {
        width: 125px;
        height: 32px;
        background: url(${(props) =>
                props.theme.palette.type === "dark" ? LightLogo : DarkLogo})
            no-repeat;
    }

    .MuiInputBase-root {
        &.MuiInput-root {
            height: 36px;
            width: 100px;
            border-radius: 4px;
            &:hover,
            & .MuiSelect-root:focus {
                border-radius: 4px;
                background-color: ${({ theme: { palette } }) =>
                    palette.type === "light"
                        ? "rgba(0, 0, 0, 0.09)"
                        : "rgba(255, 255, 255, 0.08)"};
            }
        }
        color: #fff;
        &:before,
        &:after,
        &:hover:before,
        &:hover:after {
            border-bottom: none;
        }
        .MuiSelect-root {
            display: flex;
            align-items: center;
            padding: 0 5px;
            height: 100%;
            svg {
                height: 15px;
                margin-right: 15px;
            }
        }
        .MuiSvgIcon-root {
            fill: #fff;
        }
        font-size: 14px;
        .MuiSelect-outlined {
            padding: 7px 22px 9px 12px;
        }
        .MuiSelect-iconOutlined {
            right: 0;
        }
        fieldset {
            border: none;
            &:hover {
                background-color: rgba(0, 0, 0, 0.1);
            }
        }
    }
`;

export default StyledAppBar;
