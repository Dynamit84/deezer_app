import React, { useEffect, useRef, useState } from "react";
import { connect } from "react-redux";
import GenresSelect from "./components/GenresSelect";
import { fetchData, changeGenre, resetState } from "../../../actions/actions";
import CardsList from "../../common/CardsList";
import { GENRES, ARTISTS } from "../../../constants/texts";
import { getArtistInfoUrl } from "../../../helpers/getUrls";
import ErrorBoundary from "../../common/ErrorBoundary";
import { Genre, DataForFetching } from "../../../types";
import {
    ApplicationState,
    GenresState,
    ArtistsState,
} from "../../../reducers/types";
import { useIntl } from "react-intl";

type MapStateToProps = {
    genres: GenresState;
    artists: ArtistsState;
    className?: string;
};

type MapDispatchToProps = {
    fetchData: (entity: string, params?: { id: number }) => void;
    changeGenre: (genre: Genre) => void;
    resetState: (entity: string) => void;
};

type Props = MapStateToProps & MapDispatchToProps;

const Home: React.FC<Props> = ({
    genres,
    artists,
    genres: {
        currentGenre: { id, name },
    },
    fetchData,
    changeGenre,
    resetState,
    className,
}: Props) => {
    useEffect(() => {
        fetchData(GENRES);
    }, [fetchData]);
    useEffect(() => {
        fetchData(ARTISTS, { id });
    }, [fetchData, id]);

    const myRef: React.RefObject<HTMLDivElement> = useRef(null);
    const scrollToRef = (ref: React.RefObject<HTMLDivElement>) =>
        ref && ref.current && window.scrollTo(0, ref.current.offsetTop - 64);
    const dataForFetching = {
        entity: ARTISTS,
        id,
        fetchData,
    } as DataForFetching;
    const [isSelectClicked, setIsSelectClicked] = useState(false);

    const onGenreChange = (genreName: unknown) => {
        const newGenre = genres.musicGenres.filter(
            (genre: Genre) => genre.name === genreName
        )[0];
        if (name !== newGenre.name) {
            resetState(ARTISTS);
        } else {
            scrollToRef(myRef);
        }
        setIsSelectClicked(true);
        changeGenre(newGenre);
    };

    useEffect(() => {
        if (!artists.isLoading && isSelectClicked && artists.data)
            scrollToRef(myRef);
    }, [artists.isLoading, artists.data, isSelectClicked]);

    const { formatMessage } = useIntl();

    return (
        <div className={className}>
            <div className="app-intro">
                <div className="background-img" />
                <div className={"text-container"}>
                    <h1>{formatMessage({ id: "app.home.heading" })}</h1>
                    <p>{formatMessage({ id: "app.home.text" })}</p>
                </div>
            </div>
            <div className="select-container">
                <div className="center-alignment" ref={myRef}>
                    <GenresSelect
                        {...genres}
                        onGenreChange={onGenreChange}
                        genres={formatMessage({ id: "app.genres" })}
                    />
                </div>
                <p className="select-caption">
                    {formatMessage({ id: "app.home.chooseGenre" })}
                </p>
            </div>
            <ErrorBoundary>
                <CardsList
                    data={artists}
                    linkUrl={getArtistInfoUrl}
                    dataForFetching={dataForFetching}
                />
            </ErrorBoundary>
        </div>
    );
};

const mapStateToProps = ({
    genres,
    artists,
}: ApplicationState): MapStateToProps => ({
    genres,
    artists,
});

export default connect(mapStateToProps, { fetchData, changeGenre, resetState })(
    Home
);
