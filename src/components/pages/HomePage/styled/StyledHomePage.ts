import styled from "styled-components/macro";
import Home from "../index";
import Background from "../../../../images/new-music.jpg";

const StyledHomePage = styled(Home)`
    .app-intro {
        font-family: "Exo 2", sans-serif;
        position: relative;
        height: 580px;

        .background-img {
            background: url(${Background}) no-repeat;
            background-size: cover;
            height: 580px;
            filter: blur(2px);
        }

        .text-container {
            position: absolute;
            z-index: 10;
            top: 265px;
            left: 20px;
            width: 50%;
            background-color: ${(props) =>
                props.theme.palette.background.default};
            opacity: ${({ theme: { palette } }) =>
                palette.type === "light" ? 0.7 : 1};
            padding: 20px;
            h1 {
                font-size: 40px;
                text-align: center;
            }

            p {
                font-size: 22px;
                margin: 16px 0 0;
                text-align: justify;
            }
        }
    }

    .select-container {
        display: flex;
        align-items: center;
        margin: 20px 0 20px 40px;
        height: 80px;

        .select-caption {
            font-size: 18px;
        }

        .center-alignment {
            min-width: 180px;
            display: flex;
            justify-content: center;

            .MuiFormControl-root {
                min-width: 160px;
            }
        }

        .cssload-loader {
            position: initial;
            height: 66px;
            width: 66px;

            .inner-content {
                display: none;
            }
        }
    }
    .grid {
        .grid-item {
            .title {
                font-size: 17px;
                font-weight: 600;
            }
            .actions-wrapper {
                display: none;
            }
        }
    }
`;

export default StyledHomePage;
