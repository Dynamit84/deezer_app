import React, { useEffect, useState, useRef, RefObject } from "react";
import Select from "../../../../common/Select";
import withLoader from "../../../../../hocs/withLoader";
import { Genre } from "../../../../../types";
import { FormControl } from "@material-ui/core";
import StyledComponents from "../../../../common/Select/styled";

type Props = {
    musicGenres: Genre[];
    currentGenre: Genre;
    onGenreChange: (genreName: unknown) => void;
    genres: string;
};

const GenresSelect: React.FC<Props> = ({
    musicGenres,
    currentGenre,
    onGenreChange,
    genres,
}: Props) => {
    const inputLabel: RefObject<HTMLLabelElement> = useRef(null);
    const [labelWidth, setLabelWidth] = useState<number>(0);
    useEffect(() => {
        inputLabel.current && setLabelWidth(inputLabel.current.offsetWidth);
    }, []);
    return (
        <FormControl variant="outlined">
            <StyledComponents.InputLabel ref={inputLabel} id="outlined-label">
                {genres}
            </StyledComponents.InputLabel>
            <Select
                items={musicGenres}
                selected={currentGenre.name}
                onChange={onGenreChange}
                labelWidth={labelWidth}
            />
        </FormControl>
    );
};

export default withLoader(GenresSelect);
