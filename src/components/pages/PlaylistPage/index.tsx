import React, { useEffect, useState, useRef, FC } from "react";
import { connect } from "react-redux";
import { fetchData, clickedPlay } from "../../../actions/actions";
import { PLAYLIST } from "../../../constants/texts";
import { withRouter, RouteComponentProps } from "react-router";
import { getUserUrl } from "../../../helpers/getUrls";
import { formPlaylistInfoData } from "../../../helpers/formatData";
import TracksTable from "../../common/TracksTable";
import BriefInfo from "../../common/BriefInfo";
import { TextField, Container } from "./styled";
import * as JsSearch from "js-search";
import { Loader } from "../../common/Loader";
import { FetchData, Track } from "../../../types";
import { useIntl } from "react-intl";
import { ItemInfo } from "../../../actions/types";

const tableColumns = ["number", "track", "artist", "album", "length"];

type MapStateToProps = {
    playlist: {
        id: number;
        nb_tracks: number;
        creation_date: string;
        duration: number;
        fans: number;
        tracks: {
            data: Track[];
            isLoading: boolean;
        };
        isLoading: boolean;
        creator: {
            id: number;
            name: string;
        };
        title: string;
        cover_medium: string;
    };
    playingTrackId: number;
    playingEntityId: number | null;
    isPlaying: boolean;
};

type MapDispatchToProps = {
    fetchData: FetchData;
    clickedPlay: ({ trackId, type, entityId }: Partial<ItemInfo>) => void;
};

type PlaylistParams = {
    id: string;
};

type Props = MapStateToProps &
    MapDispatchToProps &
    RouteComponentProps<PlaylistParams>;

const PlaylistPage: FC<Props> = (props) => {
    const {
        match,
        fetchData,
        playlist,
        playingTrackId,
        playingEntityId,
        isPlaying,
        clickedPlay,
    } = props;
    const search = useRef(new JsSearch.Search("title"));
    const [filteredTracks, setFilteredTracks] = useState(playlist.tracks.data);
    const { formatMessage } = useIntl();

    useEffect(() => {
        fetchData(PLAYLIST, { id: Number(match.params.id) });
    }, [fetchData, match.params.id]);

    useEffect(() => {
        setFilteredTracks(playlist.tracks.data);
        search.current.addIndex(["artist", "name"]);
        search.current.addIndex("title");
        search.current.addIndex(["album", "title"]);
        search.current.addDocuments(playlist.tracks.data);
    }, [playlist.tracks.data]);

    const filterPlaylist = (event: {}) => {
        const e = event as React.ChangeEvent<
            HTMLInputElement | HTMLInputElement
        >;
        setFilteredTracks(
            e.target.value
                ? (search.current.search(e.target.value) as Track[])
                : playlist.tracks.data
        );
    };

    return (
        <Container className="playlist-container">
            {playlist.isLoading ? (
                <Loader />
            ) : (
                <>
                    <BriefInfo
                        infoListData={formPlaylistInfoData(
                            playlist,
                            formatMessage
                        )}
                        linkUrl={getUserUrl(playlist.creator.id)}
                        entity={playlist}
                        playingEntityId={playingEntityId}
                        isPlaying={isPlaying}
                        clickedPlay={clickedPlay}
                    />
                    <TextField
                        id="outlined-search"
                        label={formatMessage({ id: "app.searchLabel" })}
                        type="search"
                        variant="outlined"
                        className="search"
                        onChange={filterPlaylist}
                    />
                    <TracksTable
                        tracks={{
                            data: filteredTracks,
                            isLoading: false,
                        }}
                        columns={tableColumns}
                        entityId={playlist.id}
                    />
                </>
            )}
        </Container>
    );
};

const mapStateToProps = ({
    playlist,
    currentlyPlaying: { id, entityId, isPlaying },
}: any): MapStateToProps => ({
    playlist,
    playingTrackId: id,
    playingEntityId: entityId,
    isPlaying,
});

export default withRouter(
    connect(mapStateToProps, { fetchData, clickedPlay })(PlaylistPage)
);
