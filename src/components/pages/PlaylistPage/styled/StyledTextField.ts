import styled from "styled-components/macro";
import TextField from "@material-ui/core/TextField";
import { fade } from "@material-ui/core/styles";

const StyledTextField = styled(TextField)`
    width: 265px;
    margin-bottom: 25px;
    background-color: ${(props) =>
        fade(props.theme.palette.common.white, 0.15)};
    border-radius: 4px;
    &:hover {
        background-color: ${(props) =>
            fade(props.theme.palette.common.white, 0.25)};
    }
    .MuiInputLabel {
        &-root {
            font-size: 14px;
            color: ${({ theme: { palette } }) =>
                palette.type === "light"
                    ? "rgba(0, 0, 0, 0.54)"
                    : palette.common.white};
        }
        &-outlined:not(.MuiInputLabel-shrink) {
            transform: translate(14px, 14px) scale(1);
        }
    }
    .MuiInputBase-root {
        height: 40px;
        .MuiInputBase-input {
            padding: 0 14px;
        }
    }
`;

export default StyledTextField;
