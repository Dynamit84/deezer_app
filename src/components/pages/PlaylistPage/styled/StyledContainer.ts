import styled from "styled-components/macro";

const StyledContainer = styled.div`
    .cssload-loader {
        top: 260px;
    }
`;

export default StyledContainer;
