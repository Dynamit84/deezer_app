import React from "react";
import { formatNumber } from "../../../helpers/formatData";
import Typography from "@material-ui/core/Typography";
import withLoader from "../../../hocs/withLoader";
import { useIntl } from "react-intl";

type Props = {
    info: {
        name: string;
        picture_medium: string;
    };
    followers?: number | null;
    followings?: number | null;
};

export default withLoader(
    ({ info, followers, followings }: Props): JSX.Element => {
        const { formatMessage } = useIntl();
        return (
            <>
                <div className="cover-container">
                    <figure className="entity-cover">
                        <img src={info.picture_medium} alt={info.name} />
                    </figure>
                </div>
                <div className="info-container">
                    <Typography variant="h4">{info.name}</Typography>
                    <div className="subtitle-container">
                        <div className="subtitle-elem">
                            <span className="subtitle-elem-text">
                                {formatMessage({ id: "app.followers" })}
                            </span>
                            <span className="subtitle-elem-number">
                                {formatNumber(followers as number)}
                            </span>
                        </div>
                        <div className="subtitle-elem">
                            <span className="subtitle-elem-text">
                                {formatMessage({ id: "app.following" })}
                            </span>
                            <span className="subtitle-elem-number">
                                {formatNumber(followings as number)}
                            </span>
                        </div>
                    </div>
                </div>
            </>
        );
    }
);
