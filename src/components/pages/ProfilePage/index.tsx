import React, { useEffect, useRef, FC } from "react";
import { connect } from "react-redux";
import { fetchData, resetState } from "../../../actions/actions";
import { withRouter, RouteComponentProps } from "react-router";
import NavBar from "../../common/NavBar";
import ProfilePreview from "./ProfilePreview";
import {
    FOLLOWING,
    FOLLOWERS,
    PROFILE,
    ALBUMS,
    USER_PLAYLISTS,
    USER_ARTISTS,
} from "../../../constants/texts";
import "./styles.scss";
import { ApplicationState, ProfileState } from "../../../reducers/types";
import { FetchData } from "../../../types";

type MapStateToProps = {
    profile: ProfileState;
};

type MapDispatchToProps = {
    fetchData: FetchData;
    resetState: (entity: string) => void;
};

type ProfileDetailParams = {
    id: string;
};

type Props = MapStateToProps &
    MapDispatchToProps &
    RouteComponentProps<ProfileDetailParams>;

const ProfilePage: FC<Props> = (props: Props) => {
    const {
        match: {
            params: { id },
        },
        fetchData,
        profile,
        profile: {
            info: { data, isLoading },
            followings,
            followers,
        },
        resetState,
    } = props;
    useEffect(() => {
        const num = Number(id);
        fetchData(PROFILE, { id: num });
        fetchData(FOLLOWERS, { id: num });
        fetchData(FOLLOWING, { id: num });
    }, [fetchData, id]);

    const pageTabs = [
        USER_PLAYLISTS,
        USER_ARTISTS,
        ALBUMS,
        FOLLOWING,
        FOLLOWERS,
    ];

    useEffect(() => {
        return () => {
            resetState(PROFILE);
        };
    }, [resetState]);

    const usePrevious = (value: string) => {
        const ref = useRef<string | null>(null);
        useEffect(() => {
            ref.current = value;
        });
        return ref.current;
    };

    const prevId = usePrevious(id);

    useEffect(() => {
        if (prevId && id !== prevId) {
            resetState(PROFILE);
        }
    }, [id, prevId, resetState]);

    return (
        <div className="profile-container">
            <div className="entity-preview">
                <ProfilePreview
                    info={data}
                    followers={followers.total}
                    followings={followings.total}
                    isLoading={isLoading}
                />
            </div>
            <NavBar
                pageTabs={pageTabs}
                id={Number(id)}
                currentView={profile}
                fetchData={fetchData}
            />
        </div>
    );
};

const mapStateToProps = ({ profile }: ApplicationState): MapStateToProps => ({
    profile,
});

export default withRouter(
    connect(mapStateToProps, { fetchData, resetState })(ProfilePage)
);
