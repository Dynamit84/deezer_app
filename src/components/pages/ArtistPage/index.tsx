import React, { useEffect, useRef, FC } from "react";
import { connect } from "react-redux";
import { fetchData, resetState } from "../../../actions/actions";
import { withRouter, RouteComponentProps } from "react-router";
import { ARTIST_INFO, ARTIST } from "../../../constants/texts";
import "./styles.scss";
import { CurrentArtistState, ApplicationState } from "../../../reducers/types";
import { FetchData } from "../../../types";
import ArtistPage from "./components/ArtistPage";

type MapStateToProps = {
    currentArtist: CurrentArtistState;
};

type MapDispatchToProps = {
    fetchData: FetchData;
    resetState: (entity: string) => void;
};

type ProfileDetailParams = {
    id: string;
};

type Props = MapStateToProps &
    MapDispatchToProps &
    RouteComponentProps<ProfileDetailParams>;

const ArtistPageContainer: FC<Props> = (props) => {
    const {
        match: {
            params: { id },
        },
        fetchData,
        resetState,
        currentArtist: {
            info: { data, isLoading },
        },
        currentArtist,
    } = props;
    const numId = Number(id);
    useEffect(() => {
        fetchData(ARTIST_INFO, { id: numId });
    }, [fetchData, numId]);

    useEffect(() => {
        return () => {
            resetState(ARTIST);
        };
    }, [resetState]);

    const usePrevious = (value: number) => {
        const ref = useRef<number | null>();
        useEffect(() => {
            ref.current = value;
        });
        return ref.current;
    };

    const prevId = usePrevious(numId);

    useEffect(() => {
        if (prevId && numId !== prevId) {
            resetState(ARTIST);
        }
    }, [numId, prevId, resetState]);

    return (
        <ArtistPage
            currentArtist={currentArtist}
            isLoading={isLoading}
            data={data}
            fetchData={fetchData}
            numId={numId}
        />
    );
};

const mapStateToProps = ({
    currentArtist,
}: ApplicationState): MapStateToProps => ({
    currentArtist,
});

export default withRouter(
    connect(mapStateToProps, { fetchData, resetState })(ArtistPageContainer)
);
