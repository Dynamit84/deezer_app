import React from "react";
import { formatNumber } from "../../../../helpers/formatData";
import Typography from "@material-ui/core/Typography";
import withLoader from "../../../../hocs/withLoader";
import { Artist } from "../../../../types";
import { useIntl } from "react-intl";

type Props = {
    data: Artist;
};

const ArtistPreview: React.FC<Props> = ({ data }: Props) => {
    const { formatMessage } = useIntl();
    return (
        <>
            <div className="cover-container">
                <figure className="entity-cover">
                    <img src={data.picture_medium} alt={data.name} />
                </figure>
            </div>
            <div className="info-container">
                <Typography variant="h4">{data.name}</Typography>
                <Typography>
                    {formatNumber(data.nb_fan as number)}{" "}
                    {formatMessage({ id: "app.fans" })}
                </Typography>
            </div>
        </>
    );
};

export default withLoader(ArtistPreview);
