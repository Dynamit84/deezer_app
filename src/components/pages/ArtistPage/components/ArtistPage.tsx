import React from "react";
import NavBar from "../../../common/NavBar";
import ArtistPreview from "./../components/ArtistPreview";
import {
    DISCOGRAPHY,
    COMMENTS,
    SIMILAR,
    PLAYLISTS,
    MOSTPLAYED,
} from "../../../../constants/texts";
import { FetchData, Artist } from "../../../../types";
import { CurrentView } from "../../../common/NavBar";

const pageTabs = [DISCOGRAPHY, MOSTPLAYED, SIMILAR, PLAYLISTS, COMMENTS];

type Props = {
    isLoading: boolean;
    currentArtist: Partial<CurrentView>;
    numId: number;
    data: Artist;
    fetchData: FetchData;
};

export default function ArtistPage({
    isLoading,
    currentArtist,
    numId,
    data,
    fetchData,
}: Props): React.ReactElement {
    return (
        <div className="artist-container">
            <div className="entity-preview">
                <ArtistPreview data={data} isLoading={isLoading} />
            </div>
            <NavBar
                pageTabs={pageTabs}
                id={numId}
                currentView={currentArtist}
                fetchData={fetchData}
            />
        </div>
    );
}
