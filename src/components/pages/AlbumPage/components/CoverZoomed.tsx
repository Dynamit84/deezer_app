import React from "react";
import CloseIcon from "@material-ui/icons/Close";
import RoundButton from "../../../common/Buttons/RoundButton";
import Modal from "../../../common/Modal/Modal";

export type Props = {
    isModalOpen: boolean;
    onModalInteract: () => void;
    imgLink: string;
    alt: string;
};

const AlbumCoverZoomed: React.FC<Props> = ({
    isModalOpen,
    onModalInteract,
    imgLink,
    alt,
}: Props) => {
    return (
        <Modal isOpen={isModalOpen} handleClose={onModalInteract}>
            <div className="modal-dialog">
                <RoundButton onClick={onModalInteract} btnClass="close">
                    <CloseIcon fontSize="small" />
                </RoundButton>
                <div>
                    <img src={imgLink} alt={alt} />
                </div>
            </div>
        </Modal>
    );
};

export default AlbumCoverZoomed;
