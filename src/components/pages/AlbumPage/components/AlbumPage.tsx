import React from "react";
import TracksTable from "../../../common/TracksTable";
import { getArtistInfoUrl } from "../../../../helpers/getUrls";
import BriefInfo from "../../../common/BriefInfo";
import { Loader } from "../../../common/Loader";
import { formAlbumInfoData } from "../../../../helpers/formatData";
import CoverZoomed from "./CoverZoomed";
import { useIntl } from "react-intl";
import { ItemInfo } from "../../../../actions/types";

type Props = {
    album: any;
    isModalOpen: boolean;
    onModalInteract: () => void;
    playingEntityId: number | null;
    isPlaying: boolean;
    clickedPlay: ({ trackId, type, entityId }: Partial<ItemInfo>) => void;
};

const tableColumns = ["number", "track", "length"];

const AlbumPage: React.FC<Props> = ({
    isModalOpen,
    onModalInteract,
    album,
    playingEntityId,
    isPlaying,
    clickedPlay,
}) => {
    const { formatMessage } = useIntl();
    return (
        <div className="album-container">
            {album.isLoading ? (
                <Loader />
            ) : (
                <>
                    <CoverZoomed
                        isModalOpen={isModalOpen}
                        onModalInteract={onModalInteract}
                        imgLink={album.cover_big}
                        alt={album.artist.name}
                    />
                    <BriefInfo
                        infoListData={formAlbumInfoData(album, formatMessage)}
                        linkUrl={getArtistInfoUrl(album.artist.id)}
                        entity={album}
                        onModalInteract={onModalInteract}
                        playingEntityId={playingEntityId}
                        isPlaying={isPlaying}
                        clickedPlay={clickedPlay}
                    />
                    <TracksTable
                        tracks={album.tracks}
                        columns={tableColumns}
                        entityId={album.id}
                    />
                </>
            )}
        </div>
    );
};

export default AlbumPage;
