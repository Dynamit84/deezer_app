import React, { useEffect, useState, ComponentType } from "react";
import { connect } from "react-redux";
import { fetchData, clickedPlay } from "../../../actions/actions";
import { ALBUM } from "../../../constants/texts";
import { withRouter, RouteComponentProps } from "react-router";
import { compose } from "redux";
import AlbumPage from "./components/AlbumPage";
import "./styles.scss";
import { Album } from "../../../types";
import { ApplicationState } from "../../../reducers/types";
import { ItemInfo } from "../../../actions/types";

type MapStateToProps = {
    album: Album;
    playingTrackId: number;
    playingEntityId: number | null;
    isPlaying: boolean;
};

type AlbumDetailParams = {
    id: string;
};

type MapDispatchToProps = {
    fetchData: (entity: string, params?: { id: string }) => void;
    clickedPlay: ({ trackId, type, entityId }: Partial<ItemInfo>) => void;
};

type Props = MapStateToProps &
    MapDispatchToProps &
    RouteComponentProps<AlbumDetailParams>;

const AlbumPageContainer: React.FC<Props> = (props) => {
    const {
        match,
        fetchData,
        album,
        playingEntityId,
        isPlaying,
        clickedPlay,
    } = props;
    useEffect(() => {
        fetchData(ALBUM, { id: match.params.id });
    }, [fetchData, match.params.id]);
    const [isModalOpen, setIsModalOpen] = useState(false);

    const onModalInteract = () => {
        setIsModalOpen(!isModalOpen);
    };

    return (
        <AlbumPage
            isModalOpen={isModalOpen}
            album={album}
            onModalInteract={onModalInteract}
            playingEntityId={playingEntityId}
            isPlaying={isPlaying}
            clickedPlay={clickedPlay}
        />
    );
};

const mapStateToProps = ({
    album,
    currentlyPlaying: { trackId, entityId, isPlaying },
}: ApplicationState): MapStateToProps => ({
    album,
    playingTrackId: trackId,
    playingEntityId: entityId,
    isPlaying,
});

const enhance = compose<ComponentType>(
    withRouter,
    connect(mapStateToProps, { fetchData, clickedPlay })
);

export default enhance(AlbumPageContainer);
