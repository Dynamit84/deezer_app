import React from "react";
import { connect } from "react-redux";
import NavBar from "../../common/NavBar";
import { fetchData } from "../../../actions/actions";
import {
    SEARCH_ALBUMS,
    SEARCH_ARTISTS,
    SEARCH_PLAYLISTS,
    SEARCH_TRACKS,
} from "../../../constants/texts";
import { withRouter, RouteComponentProps } from "react-router";
import { Redirect } from "react-router-dom";
import { Typography } from "@material-ui/core";
import "./styles.scss";
import { ApplicationState } from "../../../reducers/types";
import { FetchData } from "../../../types";
import { CurrentView } from "../../common/NavBar";
import { useIntl } from "react-intl";

type MapStateToProps = {
    search: Partial<CurrentView>;
};

type MapDispatchToProps = {
    fetchData: FetchData;
};

type Props = MapStateToProps &
    MapDispatchToProps &
    RouteComponentProps<{}, {}, { entity: string }>;

const SearchPage: React.FC<Props> = (props) => {
    const {
        search,
        location: { state },
        fetchData,
    } = props;

    const pageTabs = [
        SEARCH_ARTISTS,
        SEARCH_PLAYLISTS,
        SEARCH_TRACKS,
        SEARCH_ALBUMS,
    ];

    const { formatMessage } = useIntl();

    return state ? (
        <div className="search-container">
            <Typography variant="h5">
                {formatMessage({ id: "app.search.results" })}
            </Typography>
            <NavBar
                pageTabs={pageTabs}
                currentView={search}
                currentTab={state.entity}
                fetchData={fetchData}
            />
        </div>
    ) : (
        <Redirect to="/" />
    );
};

const mapStateToProps = ({ search }: ApplicationState): MapStateToProps => ({
    search,
});

export default withRouter(connect(mapStateToProps, { fetchData })(SearchPage));
