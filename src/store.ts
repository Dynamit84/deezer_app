import {
    createStore,
    applyMiddleware,
    compose,
    combineReducers,
    Reducer,
} from "redux";
import genresReducer from "./reducers/genresReducer";
import artistsReducer from "./reducers/artistsReducer";
import currentArtistReducer from "./reducers/currentArtistReducer";
import currentlyPlayingReducer from "./reducers/currentlyPlayingReducer";
import playerReducer from "./reducers/playerReducer";
import albumReducer from "./reducers/albumReducer";
import playlistReducer from "./reducers/playlistReducer";
import profileReducer from "./reducers/profileReducer";
import searchReducer from "./reducers/searchReducer";
import createSagaMiddleware from "redux-saga";
import rootSaga from "./sagas/rootSaga";
import { ApplicationState } from "./reducers/types";
import userReducer from "./reducers/userReducer";
import localStorage from "./helpers/localStorage";
import { storageMiddleware } from "./middlewares";

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
    }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const sagaMiddleware = createSagaMiddleware();
const reducers: Reducer<ApplicationState> = combineReducers({
    genres: genresReducer,
    artists: artistsReducer,
    currentArtist: currentArtistReducer,
    currentlyPlaying: currentlyPlayingReducer,
    player: playerReducer,
    album: albumReducer,
    playlist: playlistReducer,
    profile: profileReducer,
    search: searchReducer,
    user: userReducer,
});
const preloadedState = { user: localStorage.loadState() };
export default createStore(
    reducers,
    preloadedState as ApplicationState,
    composeEnhancers(applyMiddleware(sagaMiddleware, storageMiddleware))
);

sagaMiddleware.run(rootSaga);
