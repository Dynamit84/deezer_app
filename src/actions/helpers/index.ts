import {
    FETCH_GENRES_SUCCESS,
    FETCH_GENRES_FAILS,
    FETCH_ARTISTS_SUCCESS,
    FETCH_ARTISTS_FAILS,
    FETCH_ARTIST_INFO_FAILS,
    FETCH_ARTIST_INFO_SUCCESS,
    FETCH_ALBUM_FAILS,
    FETCH_ALBUM_SUCCESS,
    FETCH_ARTIST_COMMENTS_SUCCESS,
    FETCH_ARTIST_COMMENTS_FAILS,
    FETCH_ARTIST_DISCOGRAPHY_FAILS,
    FETCH_ARTIST_DISCOGRAPHY_SUCCESS,
    FETCH_ARTIST_MOSTPLAYED_FAILS,
    FETCH_ARTIST_MOSTPLAYED_SUCCESS,
    FETCH_ARTIST_PLAYLISTS_FAILS,
    FETCH_ARTIST_PLAYLISTS_SUCCESS,
    FETCH_ARTIST_SIMILAR_FAILS,
    FETCH_ARTIST_SIMILAR_SUCCESS,
    FETCH_PLAYLIST_SUCCESS,
    FETCH_PLAYLIST_FAILS,
    FETCH_USER_PROFILE_FAILS,
    FETCH_USER_PROFILE_SUCCESS,
    FETCH_USER_FOLLOWERS_FAILS,
    FETCH_USER_FOLLOWERS_SUCCESS,
    FETCH_USER_FOLLOWINGS_FAILS,
    FETCH_USER_FOLLOWINGS_SUCCESS,
    FETCH_USER_ALBUMS_SUCCESS,
    FETCH_USER_ALBUMS_FAILS,
    FETCH_USER_PLAYLISTS_SUCCESS,
    FETCH_USER_PLAYLISTS_FAILS,
    FETCH_USER_ARTISTS_FAILS,
    FETCH_USER_ARTISTS_SUCCESS,
    FETCH_ARTIST_DISCOGRAPHY,
    FETCH_ARTIST_INFO,
    FETCH_ARTIST_COMMENTS,
    FETCH_ARTISTS,
    FETCH_GENRES,
    FETCH_ARTIST_PLAYLISTS,
    FETCH_ARTIST_MOSTPLAYED,
    FETCH_ARTIST_SIMILAR,
    FETCH_PLAYLIST,
    FETCH_USER_ALBUMS,
    FETCH_USER_ARTISTS,
    FETCH_USER_FOLLOWERS,
    FETCH_USER_FOLLOWINGS,
    FETCH_USER_PLAYLISTS,
    FETCH_USER_PROFILE,
    FETCH_SEARCH_ALBUMS,
    FETCH_SEARCH_ALBUMS_FAILS,
    FETCH_SEARCH_ALBUMS_SUCCESS,
    FETCH_SEARCH_ARTISTS,
    FETCH_SEARCH_ARTISTS_FAILS,
    FETCH_SEARCH_ARTISTS_SUCCESS,
    FETCH_SEARCH_PLAYLISTS,
    FETCH_SEARCH_PLAYLISTS_FAILS,
    FETCH_SEARCH_PLAYLISTS_SUCCESS,
    FETCH_SEARCH_TRACKS,
    FETCH_SEARCH_TRACKS_FAILS,
    FETCH_SEARCH_TRACKS_SUCCESS,
    FETCH_SEARCH_DATA_SUCCESS,
    FETCH_SEARCH_DATA_FAILS,
    FETCH_SEARCH_DATA_IN_PROGRESS,
    FETCH_ALBUM,
} from "../actionTypes";
import {
    genresUrl,
    getArtistInfoUrl,
    getGenreArtistsUrl,
    getArtistAlbumsUrl,
    getArtistCommentsUrl,
    getArtistPlaylistsUrl,
    getArtistSimilarUrl,
    getArtistMostPlayedUrl,
    getAlbumUrl,
    getPlaylistUrl,
    getUserUrl,
    getUserFollowersUrl,
    getUserFollowingsUrl,
    getUserAlbumsUrl,
    getUserPlaylistsUrl,
    getUserArtistsUrl,
    getSearchAlbumsUrl,
    getSearchArtistsUrl,
    getSearchPlaylistsUrl,
    getSearchTracksUrl,
} from "../../helpers/getUrls";
import {
    GENRES,
    ARTISTS,
    ARTIST_INFO,
    MOSTPLAYED,
    PLAYLISTS,
    COMMENTS,
    SIMILAR,
    DISCOGRAPHY,
    ALBUM,
    PLAYLIST,
    PROFILE,
    FOLLOWING,
    FOLLOWERS,
    ALBUMS,
    USER_PLAYLISTS,
    USER_ARTISTS,
    SEARCH_TRACKS,
    SEARCH_ARTISTS,
    SEARCH_ALBUMS,
    SEARCH_PLAYLISTS,
    SEARCH_DATA,
} from "../../constants/texts";
import { Options, EntitiesHash, Payload } from "../types";

export const entitiesHash: { [key: string]: EntitiesHash } = {
    [ARTISTS]: {
        success: FETCH_ARTISTS_SUCCESS,
        fail: FETCH_ARTISTS_FAILS,
        loading: FETCH_ARTISTS,
    },
    [GENRES]: {
        success: FETCH_GENRES_SUCCESS,
        fail: FETCH_GENRES_FAILS,
        loading: FETCH_GENRES,
    },
    [ARTIST_INFO]: {
        success: FETCH_ARTIST_INFO_SUCCESS,
        fail: FETCH_ARTIST_INFO_FAILS,
        loading: FETCH_ARTIST_INFO,
    },
    [PLAYLISTS]: {
        success: FETCH_ARTIST_PLAYLISTS_SUCCESS,
        fail: FETCH_ARTIST_PLAYLISTS_FAILS,
        loading: FETCH_ARTIST_PLAYLISTS,
    },
    [COMMENTS]: {
        success: FETCH_ARTIST_COMMENTS_SUCCESS,
        fail: FETCH_ARTIST_COMMENTS_FAILS,
        loading: FETCH_ARTIST_COMMENTS,
    },
    [SIMILAR]: {
        success: FETCH_ARTIST_SIMILAR_SUCCESS,
        fail: FETCH_ARTIST_SIMILAR_FAILS,
        loading: FETCH_ARTIST_SIMILAR,
    },
    [DISCOGRAPHY]: {
        success: FETCH_ARTIST_DISCOGRAPHY_SUCCESS,
        fail: FETCH_ARTIST_DISCOGRAPHY_FAILS,
        loading: FETCH_ARTIST_DISCOGRAPHY,
    },
    [MOSTPLAYED]: {
        success: FETCH_ARTIST_MOSTPLAYED_SUCCESS,
        fail: FETCH_ARTIST_MOSTPLAYED_FAILS,
        loading: FETCH_ARTIST_MOSTPLAYED,
    },
    [ALBUM]: {
        success: FETCH_ALBUM_SUCCESS,
        fail: FETCH_ALBUM_FAILS,
        loading: FETCH_ALBUM,
    },
    [PLAYLIST]: {
        success: FETCH_PLAYLIST_SUCCESS,
        fail: FETCH_PLAYLIST_FAILS,
        loading: FETCH_PLAYLIST,
    },
    [PROFILE]: {
        success: FETCH_USER_PROFILE_SUCCESS,
        fail: FETCH_USER_PROFILE_FAILS,
        loading: FETCH_USER_PROFILE,
    },
    [FOLLOWING]: {
        success: FETCH_USER_FOLLOWINGS_SUCCESS,
        fail: FETCH_USER_FOLLOWINGS_FAILS,
        loading: FETCH_USER_FOLLOWINGS,
    },
    [FOLLOWERS]: {
        success: FETCH_USER_FOLLOWERS_SUCCESS,
        fail: FETCH_USER_FOLLOWERS_FAILS,
        loading: FETCH_USER_FOLLOWERS,
    },
    [ALBUMS]: {
        success: FETCH_USER_ALBUMS_SUCCESS,
        fail: FETCH_USER_ALBUMS_FAILS,
        loading: FETCH_USER_ALBUMS,
    },
    [USER_PLAYLISTS]: {
        success: FETCH_USER_PLAYLISTS_SUCCESS,
        fail: FETCH_USER_PLAYLISTS_FAILS,
        loading: FETCH_USER_PLAYLISTS,
    },
    [USER_ARTISTS]: {
        success: FETCH_USER_ARTISTS_SUCCESS,
        fail: FETCH_USER_ARTISTS_FAILS,
        loading: FETCH_USER_ARTISTS,
    },
    [SEARCH_TRACKS]: {
        success: FETCH_SEARCH_TRACKS_SUCCESS,
        fail: FETCH_SEARCH_TRACKS_FAILS,
        loading: FETCH_SEARCH_TRACKS,
    },
    [SEARCH_ARTISTS]: {
        success: FETCH_SEARCH_ARTISTS_SUCCESS,
        fail: FETCH_SEARCH_ARTISTS_FAILS,
        loading: FETCH_SEARCH_ARTISTS,
    },
    [SEARCH_ALBUMS]: {
        success: FETCH_SEARCH_ALBUMS_SUCCESS,
        fail: FETCH_SEARCH_ALBUMS_FAILS,
        loading: FETCH_SEARCH_ALBUMS,
    },
    [SEARCH_PLAYLISTS]: {
        success: FETCH_SEARCH_PLAYLISTS_SUCCESS,
        fail: FETCH_SEARCH_PLAYLISTS_FAILS,
        loading: FETCH_SEARCH_PLAYLISTS,
    },
    [SEARCH_DATA]: {
        success: FETCH_SEARCH_DATA_SUCCESS,
        fail: FETCH_SEARCH_DATA_FAILS,
        loading: FETCH_SEARCH_DATA_IN_PROGRESS,
    },
};

export const getActionPayload = (
    entity: string,
    { id, params, search } = {} as Options
) => {
    let payload: Payload = {};
    switch (entity) {
        case GENRES:
            payload = {
                entity: entity,
                url: genresUrl,
            };
            break;
        case ARTISTS:
            payload = {
                entity: entity,
                url: getGenreArtistsUrl(id),
            };
            break;
        case ARTIST_INFO:
            payload = {
                entity: entity,
                url: getArtistInfoUrl(id),
            };
            break;
        case DISCOGRAPHY:
            payload = {
                entity: entity,
                url: getArtistAlbumsUrl(id, params),
            };
            break;
        case MOSTPLAYED:
            payload = {
                entity: entity,
                url: getArtistMostPlayedUrl(id),
            };
            break;
        case PLAYLISTS:
            payload = {
                entity: entity,
                url: getArtistPlaylistsUrl(id, params),
            };
            break;
        case COMMENTS:
            payload = {
                entity: entity,
                url: getArtistCommentsUrl(id, params),
            };
            break;
        case SIMILAR:
            payload = {
                entity: entity,
                url: getArtistSimilarUrl(id, params),
            };
            break;
        case ALBUM:
            payload = {
                entity: entity,
                url: getAlbumUrl(id),
            };
            break;
        case PLAYLIST:
            payload = {
                entity: entity,
                url: getPlaylistUrl(id),
            };
            break;

        case PROFILE:
            payload = {
                entity: entity,
                url: getUserUrl(id),
            };
            break;
        case FOLLOWERS:
            payload = {
                entity: entity,
                url: getUserFollowersUrl(id, params),
            };
            break;
        case FOLLOWING:
            payload = {
                entity: entity,
                url: getUserFollowingsUrl(id, params),
            };
            break;
        case ALBUMS:
            payload = {
                entity: entity,
                url: getUserAlbumsUrl(id, params),
            };
            break;
        case USER_PLAYLISTS:
            payload = {
                entity: entity,
                url: getUserPlaylistsUrl(id, params),
            };
            break;
        case USER_ARTISTS:
            payload = {
                entity: entity,
                url: getUserArtistsUrl(id, params),
            };
            break;
        case SEARCH_PLAYLISTS:
            payload = {
                entity: entity,
                url: getSearchPlaylistsUrl(search as string, params),
            };
            break;
        case SEARCH_ALBUMS:
            payload = {
                entity: entity,
                url: getSearchAlbumsUrl(search as string, params),
            };
            break;
        case SEARCH_TRACKS:
            payload = {
                entity: entity,
                url: getSearchTracksUrl(search as string, params),
            };
            break;
        case SEARCH_ARTISTS:
            payload = {
                entity: entity,
                url: getSearchArtistsUrl(search as string, params),
            };
            break;
        default:
            return payload;
    }
    return payload;
};
