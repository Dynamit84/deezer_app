import { UpdateLocalStorageAction } from "./types/index";
import { UserState } from "./../reducers/types/index";
import { ActionCreator } from "redux";
import {
    CHANGE_GENRE,
    FETCH_DATA,
    CLICKED_PLAY_BUTTON,
    PLAYER_LOADED,
    FETCH_SEARCH_DATA,
    RESET_ARIST_STATE,
    RESET_ARISTS_STATE,
    RESET_PROFILE_STATE,
    CHANGE_LANGUAGE,
    TOGGLE_THEME,
    UPDATE_LOCAL_STORAGE,
} from "./actionTypes";
import { ARTISTS, PROFILE, ARTIST } from "../constants/texts";
import {
    ChangeGenreAction,
    PlayerLoadedAction,
    ClickedPlayAction,
    ItemInfo,
    ResetStateAction,
    EntitiesStrings,
    ResetTypes,
    FetchDataSuccessAction,
    FetchDataFailsAction,
    FetchingDataAction,
    Options,
    FetchDataAction,
    FetchSearchDataAction,
    ChangeLanguageAction,
    ToggleThemeAction,
} from "./types";
import { entitiesHash, getActionPayload } from "./helpers";

const defaultOptions = {
    id: 0,
    params: "",
    search: "",
};

export const fetchSearchData: ActionCreator<FetchSearchDataAction> = (
    entities: string[],
    search: string
) => {
    const actionPayload = entities.map((entity: string) => {
        return {
            payload: getActionPayload(entity, { ...defaultOptions, search }),
        };
    });
    return { type: FETCH_SEARCH_DATA, payload: actionPayload };
};

export const fetchData: ActionCreator<FetchDataAction> = (
    entity: string,
    options: Options
) => {
    const payload = getActionPayload(entity, options);
    return { type: FETCH_DATA, payload };
};

export const fetchingData: ActionCreator<FetchingDataAction> = (
    entity: string
) => ({
    type: entitiesHash[entity].loading,
});

export const fetchDataSuccess: ActionCreator<FetchDataSuccessAction> = (
    entity: string,
    data: any
) => ({
    type: entitiesHash[entity].success,
    payload: data,
});

export const fetchDataFails: ActionCreator<FetchDataFailsAction> = (
    entity: string,
    error: any
) => ({
    type: entitiesHash[entity].fail,
    payload: error,
});

export const changeGenre: ActionCreator<ChangeGenreAction> = (
    genre: string
) => ({
    type: CHANGE_GENRE,
    payload: genre,
});

export const changeLanguage: ActionCreator<ChangeLanguageAction> = (
    language: string
) => ({
    type: CHANGE_LANGUAGE,
    payload: language,
});

export const toggleTheme: ActionCreator<ToggleThemeAction> = (
    theme: string
) => ({
    type: TOGGLE_THEME,
    payload: theme,
});

export const clickedPlay: ActionCreator<ClickedPlayAction> = (
    itemInfo: Partial<ItemInfo>
) => ({
    type: CLICKED_PLAY_BUTTON,
    payload: itemInfo,
});

export const playerLoaded: ActionCreator<PlayerLoadedAction> = () => ({
    type: PLAYER_LOADED,
});

const statesHash: { [key: string]: { reset: keyof typeof ResetTypes } } = {
    [ARTIST]: {
        reset: RESET_ARIST_STATE,
    },
    [ARTISTS]: {
        reset: RESET_ARISTS_STATE,
    },
    [PROFILE]: {
        reset: RESET_PROFILE_STATE,
    },
};

export const resetState: ActionCreator<ResetStateAction> = (
    entity: EntitiesStrings
) => ({
    type: statesHash[entity].reset,
});

export const updateLocalStorage: ActionCreator<UpdateLocalStorageAction> = (
    user: UserState
) => ({
    type: UPDATE_LOCAL_STORAGE,
    payload: user,
});
