import { UserState } from "./../../reducers/types/index";
import { Action } from "redux";

export interface ChangeGenreAction extends Action {
    type: "CHANGE_GENRE";
    payload: string;
}

export interface PlayerLoadedAction extends Action {
    type: "PLAYER_LOADED";
}

export interface UpdateLocalStorageAction extends Action {
    type: "UPDATE_LOCAL_STORAGE";
    payload: UserState;
}

export type ItemInfo = {
    trackId: number;
    type: string;
    entityId: number | null;
    isPlaying: boolean;
};

export interface ClickedPlayAction extends Action {
    type: "CLICKED_PLAY_BUTTON";
    payload?: Partial<ItemInfo>;
}

export enum ResetTypes {
    RESET_ARIST_STATE,
    RESET_ARISTS_STATE,
    RESET_PROFILE_STATE,
}

enum Entities {
    ARTIST,
    ARTISTS,
    PROFILE,
}

export type EntitiesStrings = keyof typeof Entities;

export interface ResetStateAction extends Action {
    type: keyof typeof ResetTypes;
}

export interface FetchDataFailsAction extends Action {
    type: string;
    payload: any;
}

export interface FetchDataSuccessAction extends Action {
    type: string;
    payload: any;
}

export interface FetchSearchDataAction extends Action {
    type: "FETCH_SEARCH_DATA";
    payload: any;
}

export interface FetchingDataAction extends Action {
    type: string;
}

export interface FetchDataAction extends Action {
    type: "FETCH_DATA";
    payload: Payload;
}

export interface ChangeLanguageAction extends Action {
    type: "CHANGE_LANGUAGE";
    payload: string;
}

export interface ToggleThemeAction extends Action {
    type: "TOGGLE_THEME";
}

export type EntitiesHash = {
    success: string;
    fail: string;
    loading: string;
};

export type Options = {
    id: number;
    params?: string;
    search?: string;
};

export type Payload =
    | {
          entity: string;
          url: string;
      }
    | {};

export type FetchDataActions = FetchDataSuccessAction | FetchDataFailsAction;
export type GenresActions = FetchDataActions | ChangeGenreAction;
export type FetchResetActions = FetchDataActions | ResetStateAction;
export type UserActions = ChangeLanguageAction | ToggleThemeAction;
