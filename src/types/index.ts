export type Track = {
    id: number;
    title: string;
    artist: Artist;
    album: Album;
    duration: number;
};

export type Album = {
    id: number | null;
    title: string | null;
    cover_big: string | null;
    cover_medium: string | null;
    fans: number | null;
    release_date: string | null;
    isLoading?: boolean;
    tracks?: {
        data: Track[];
    };
    artist?: Artist;
    nb_tracks?: number;
    duration?: number;
    errorMsg?: string;
};

export type Playlist = {
    id: number;
    title: string;
    creation_date: string;
    picture_medium: string;
    tracks?: {
        data: Track[];
        isLoading: boolean;
    };
    nb_tracks?: number;
    duration?: number;
    fans?: number;
    description?: string;
    creator?: {
        id: number;
        name: string;
    };
    isLoading?: boolean;
};

export type Artist = {
    name: string;
    nb_fan?: number;
    picture_medium: string;
    id: number;
};

export type Genre = {
    name: string;
    id: number;
};

export type TComment = {
    text: string;
    author: {
        name: string;
        picture_small: string;
        id: number;
    };
    id: number;
    date: number;
};

export type User = { name: string; picture_medium: string; id: number };

export type FetchData = (
    entity: string,
    params?: { id?: number; params?: string }
) => void;

export type DataForFetching = {
    id?: number;
    entity: string;
    fetchData: FetchData;
};

export type GeneralData = {
    data: Array<TComment | Album | Track | Artist | Playlist | User>;
    isLoading: boolean;
    isFirstLoad?: boolean;
    next?: string;
    errorMsg?: string;
    total?: number | null;
};

declare global {
    interface Window {
        DZ: any;
    }
}

export type LinkUrl = (id: number) => string;
